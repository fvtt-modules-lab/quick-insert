import { getSetting, registerMenu } from "module/settings";
import { setupDocumentHooks } from "module/documentHooks";

import { importSystemIntegration } from "module/systemIntegration";
import { registerTinyMCEPlugin } from "module/tinyMCEPlugin";

import { SearchAppShell } from "app/search/SearchAppShell.svelte";
import { SettingsApp } from "module/settingsApp";
import { QuickInsert, loadSearchIndex } from "module/core";
import { customKeybindHandler, resolveAfter } from "module/utils";
import { ModuleKeyBinds, ModuleSetting } from "module/store/ModuleSettings";
import { registerSettings } from "module/store/SettingsRegistry";
import { registerProseMirrorKeys } from "module/ProseMirrorKeyExtension";
import { migrateFilters } from "module/store/migrateFilters";

function quickInsertDisabled(): boolean {
  return !game.user?.isGM && getSetting(ModuleSetting.GM_ONLY);
}

// Client is currently reindexing?
let reIndexing = false;

async function reIndex() {
  if (quickInsertDisabled()) return;
  // Active users will start reindexing in deterministic order, once per 300ms

  if (reIndexing) return;
  reIndexing = true;
  if (game.users && game.userId !== null) {
    const order = [...game.users.contents]
      //@ts-expect-error not sure why it's missing
      .filter((u) => u.active)
      .map((u) => u.id)
      .indexOf(game.userId);
    await resolveAfter(order * 300);
  }
  await QuickInsert.forceIndex();
  reIndexing = false;
}

Hooks.once("init", async function () {
  registerSettings({
    [ModuleSetting.INDEXING_DISABLED]: () => reIndex(),
    [ModuleSetting.SEARCH_ENGINE]: () => reIndex(),
  });

  game.keybindings.register("quick-insert", ModuleKeyBinds.TOGGLE_OPEN, {
    name: "QUICKINSERT.SettingsQuickOpen",
    textInput: true,
    editable: [
      { key: "Space", modifiers: [KeyboardManager.MODIFIER_KEYS.CONTROL] },
    ],
    onDown: (ctx: any) => {
      QuickInsert.toggle(ctx._quick_insert_extra?.context);
      return true;
    },
    precedence: CONST.KEYBINDING_PRECEDENCE.NORMAL,
  });

  game.keybindings.register("quick-insert", ModuleKeyBinds.OPEN_SETTINGS, {
    name: "QUICKINSERT.SettingsMenuLabel",
    textInput: true,
    editable: [],
    onDown: () => {
      new SettingsApp().render(true);
    },
    precedence: CONST.KEYBINDING_PRECEDENCE.NORMAL,
  });
});

Hooks.once("ready", function () {
  if (quickInsertDisabled()) return;

  registerMenu({
    namespace: "quick-insert",
    menu: "quickInsertSettings",
    key: "quickInsertSettings",
    name: "QUICKINSERT.SettingsMenu",
    label: "QUICKINSERT.SettingsMenuLabel",
    icon: "fas fa-sliders-h",
    type: SettingsApp,
    restricted: false,
  });

  console.log("Quick Insert | Initializing...");

  migrateFilters();

  QuickInsert.filters.init();
  QuickInsert.app = new SearchAppShell();

  registerTinyMCEPlugin();
  registerProseMirrorKeys();

  importSystemIntegration().then((systemIntegration) => {
    if (systemIntegration) {
      QuickInsert.systemIntegration = systemIntegration;
      QuickInsert.systemIntegration.init();
    }
  });

  document.addEventListener("keydown", (evt) => {
    // Allow in input fields...
    customKeybindHandler(evt);
  });

  document.addEventListener("click", (evt) => {
    if (
      evt.target &&
      QuickInsert.app?.rendered &&
      "closest" in evt.target &&
      !evt.shiftKey
    ) {
      const target = evt.target as Element;
      if (
        target.closest &&
        !target.closest(".quick-insert-app") &&
        !target.closest(".quick-insert-result")
      ) {
        QuickInsert.app?.clickOutside();
      }
    }
  });
  setupDocumentHooks(QuickInsert);

  console.log("Quick Insert | Search Application ready");

  const indexDelay = getSetting(ModuleSetting.AUTOMATIC_INDEXING);
  if (indexDelay != -1) {
    setTimeout(() => {
      console.log("Quick Insert | Automatic indexing initiated");
      loadSearchIndex();
    }, indexDelay);
  }
});

Hooks.on(
  "renderSceneControls",
  (controls: unknown, html: JQuery | HTMLElement) => {
    if (quickInsertDisabled() || !getSetting(ModuleSetting.SEARCH_BUTTON)) {
      return;
    }
    if (!(html instanceof HTMLElement)) {
      // Pre 13.x
      const searchBtn = $(
        `<li class="scene-control" title="Quick Insert" class="quick-insert-open">
            <i class="fas fa-search"></i>
        </li>`
      );
      html.children(".main-controls").append(searchBtn);
      searchBtn.on("click", () => QuickInsert.open());
    } else {
      // Post 13.x
      if (
        html.querySelector(".quick-insert-control") ||
        !html.firstElementChild
      ) {
        return;
      }
      const button = `<li><button type="button" class="control ui-control layer icon fa-solid fa-search quick-insert-control" role="tab" data-action="control" data-control="" data-tooltip="Quick Insert" aria-pressed="false" aria-label="Quick Insert" aria-controls="scene-controls-tools"></button></li>`;
      html.firstElementChild.insertAdjacentHTML("beforeend", button);
      html.firstElementChild.lastElementChild?.addEventListener("click", () =>
        QuickInsert.open()
      );
    }
  }
);

// Exports and API usage
//@ts-expect-error not defined
globalThis.QuickInsert = QuickInsert;
export { SearchContext } from "module/contexts";
export { QuickInsert };
