import { SearchLib, type SearchResult } from "./searchLib";
import type { SearchAppShell } from "app/search/SearchAppShell.svelte";
import { SearchFilterCollection } from "./searchFilters";
import type { SearchContext } from "./contexts";
import type { SystemIntegration } from "./systemIntegration";
import { customKeybindHandler } from "./utils";

// Module singleton class that contains everything
class QuickInsertCore {
  app?: SearchAppShell;
  searchLib?: SearchLib;
  filters = new SearchFilterCollection();
  systemIntegration?: SystemIntegration;

  public get hasIndex(): boolean {
    return Boolean(this.searchLib?.index);
  }

  /**
   * Incorrect to match like this with new keybinds!
   * @deprecated
   */
  matchBoundKeyEvent(): boolean {
    return false;
  }

  // If the global key binds are not enough - e.g. in a custom editor,
  // include the custom search context!
  handleKeybind(evt: KeyboardEvent, context: SearchContext): void {
    if (!context) throw new Error("A custom context is required!");
    customKeybindHandler(evt, context);
  }

  open(context?: SearchContext): void {
    this.app?.render({ context });
  }

  toggle(context?: SearchContext): void {
    if (this.app?.rendered) {
      this.app.close();
    } else {
      this.open(context);
    }
  }

  search(text: string, filter = null, max = 100): SearchResult[] {
    return this.searchLib?.search(text, filter, max) || [];
  }

  async forceIndex(): Promise<void> {
    return loadSearchIndex();
  }
}

export const QuickInsert = new QuickInsertCore();

// Ensure that only one loadSearchIndex function is running at any one time.
let isLoading = false;
export async function loadSearchIndex(): Promise<void> {
  if (isLoading) return;
  isLoading = true;
  console.log("Quick Insert | Preparing search index...");
  const start = performance.now();
  QuickInsert.searchLib = new SearchLib();
  QuickInsert.searchLib.indexDocuments();

  console.log(`Quick Insert | Indexing compendiums`);
  await QuickInsert.searchLib.indexCompendiums();

  console.log(
    `Quick Insert | Search index and filters completed. Indexed ${
      QuickInsert.searchLib?.index.getSize() || 0
    } items in ${performance.now() - start}ms`
  );

  isLoading = false;
  Hooks.callAll("QuickInsert:IndexCompleted", QuickInsert);
}
