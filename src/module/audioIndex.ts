import fuzzysort from "fuzzysort-esm";

function getContents() {
  return CONFIG.Playlist.collection.instance.contents
    .filter((i) => i.visible)
    .map((c) => {
      // typing doesn't understand the collection instance type
      const instance = c as unknown as Playlist;
      return {
        instance,
        track: instance.playing
          ? instance.sounds.find((s) => s.playing)?.name || undefined
          : undefined,
      };
    });
}

export function searchPlaylists(query: string) {
  if (!query) {
    return getContents().map((item) => ({
      item,
    }));
  }

  return fuzzysort
    .go(query, getContents(), {
      key: (i) => i.instance.name,
      all: true,
      threshold: 0.5,
    })
    .map((res) => {
      return {
        item: res.obj,
        formattedMatch: res.highlight("<strong>", "</strong>"),
      };
    });
}
