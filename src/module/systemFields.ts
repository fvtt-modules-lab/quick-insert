import type { DocumentType } from "./searchLib";

export interface SystemFieldConfig {
  documentType: DocumentType;
  indexName: string;
  fieldTitle: string;
}

export const systemFields: SystemFieldConfig[] = [];

const memoized: Record<string, SystemFieldConfig[]> = {};
export function getSystemFields(type: DocumentType) {
  if (memoized[type]) {
    return memoized[type];
  }
  memoized[type] = systemFields.filter((f) => f.documentType === type);
  return memoized[type];
}
