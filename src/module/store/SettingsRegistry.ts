import { registerSetting } from "../settings";
import type { IndexingDisabledSetting } from "./IndexingDisabledType";
import { ModuleSetting, SAVE_SETTINGS_REVISION } from "./ModuleSettings";
import { stores } from "./SettingsStore";

function getModuleSettings(): Record<
  ModuleSetting,
  any /* ClientSettings.PartialSetting */
> {
  return {
    [ModuleSetting.GM_ONLY]: {
      name: "QUICKINSERT.SettingsGmOnly",
      hint: "QUICKINSERT.SettingsGmOnlyHint",
      type: Boolean,
      default: false,
      scope: "world",
    },
    [ModuleSetting.FILTERS_SHEETS_ENABLED]: {
      name: "QUICKINSERT.SettingsFiltersSheetsEnabled",
      hint: "QUICKINSERT.SettingsFiltersSheetsEnabledHint",
      type: Boolean,
      default: true,
      scope: "world",
    },
    [ModuleSetting.AUTOMATIC_INDEXING]: {
      name: "QUICKINSERT.SettingsAutomaticIndexing",
      hint: "QUICKINSERT.SettingsAutomaticIndexingHint",
      type: Number,
      choices: {
        3000: "QUICKINSERT.SettingsAutomaticIndexing3s",
        5000: "QUICKINSERT.SettingsAutomaticIndexing5s",
        10000: "QUICKINSERT.SettingsAutomaticIndexing10s",
        "-1": "QUICKINSERT.SettingsAutomaticIndexingOnFirstOpen",
      },
      default: -1,
      scope: "world",
    },
    [ModuleSetting.SEARCH_BUTTON]: {
      name: "QUICKINSERT.SettingsSearchButton",
      hint: "QUICKINSERT.SettingsSearchButtonHint",
      type: Boolean,
      default: false,
      scope: "world",
    },
    [ModuleSetting.ENABLE_GLOBAL_CONTEXT]: {
      name: "QUICKINSERT.SettingsEnableGlobalContext",
      hint: "QUICKINSERT.SettingsEnableGlobalContextHint",
      type: Boolean,
      default: true,
    },
    [ModuleSetting.DEFAULT_ACTION_SCENE]: {
      name: "QUICKINSERT.SettingsDefaultActionScene",
      hint: "QUICKINSERT.SettingsDefaultActionSceneHint",
      type: String,
      choices: {
        show: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.Configure"
          : "SCENES.Configure",
        viewScene: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.View"
          : "SCENES.View",
        activateScene: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.Activate"
          : "SCENES.Activate",
      },
      default: "show",
    },
    [ModuleSetting.DEFAULT_ACTION_ROLL_TABLE]: {
      name: "QUICKINSERT.SettingsDefaultActionRollTable",
      hint: "QUICKINSERT.SettingsDefaultActionRollTableHint",
      type: String,
      choices: {
        show: "QUICKINSERT.ActionEdit",
        roll: foundry.utils.isNewerVersion(game.version, "13")
          ? "TABLE.ACTIONS.DrawResult"
          : "TABLE.Roll",
      },
      default: "show",
    },
    [ModuleSetting.DEFAULT_ACTION_MACRO]: {
      name: "QUICKINSERT.SettingsDefaultActionMacro",
      hint: "QUICKINSERT.SettingsDefaultActionMacroHint",
      type: String,
      choices: {
        show: "QUICKINSERT.ActionEdit",
        execute: "QUICKINSERT.ActionExecute",
      },
      default: "show",
    },
    [ModuleSetting.SEARCH_TOOLTIPS]: {
      setting: ModuleSetting.SEARCH_TOOLTIPS,
      name: "QUICKINSERT.SettingsSearchTooltips",
      hint: "QUICKINSERT.SettingsSearchTooltipsHint",
      type: String,
      choices: {
        off: "QUICKINSERT.SettingsSearchTooltipsValueOff",
        text: "QUICKINSERT.SettingsSearchTooltipsValueText",
        image: "QUICKINSERT.SettingsSearchTooltipsValueImage",
        full: "QUICKINSERT.SettingsSearchTooltipsValueFull",
      },
      default: "text",
    },
    [ModuleSetting.EMBEDDED_INDEXING]: {
      setting: ModuleSetting.EMBEDDED_INDEXING,
      name: "QUICKINSERT.SettingsEmbeddedIndexing",
      hint: "QUICKINSERT.SettingsEmbeddedIndexingHint",
      type: Boolean,
      default: false,
      scope: "world",
    },
    [ModuleSetting.INDEXING_DISABLED]: {
      name: "Things that have indexing disabled",
      type: Object,
      default: {
        entities: {
          Macro: [1, 2],
          Scene: [1, 2],
          Playlist: [1, 2],
          RollTable: [1, 2],
        },
        packs: {},
      } as IndexingDisabledSetting,
      scope: "world",
    },
    [ModuleSetting.FILTERS_CLIENT]: {
      name: "Own filters",
      type: Object,
      default: {
        saveRev: SAVE_SETTINGS_REVISION,
        disabled: [],
        filters: [],
      },
    },
    [ModuleSetting.FILTERS_WORLD]: {
      name: "World filters",
      type: Object,
      default: {
        saveRev: SAVE_SETTINGS_REVISION,
        filters: [],
      },
      scope: "world",
    },
    [ModuleSetting.FILTERS_ADD_DEFAULT_SUBTYPE]: {
      name: "Add default filters for subtypes",
      type: Boolean,
      default: true,
      scope: "world",
    },
    [ModuleSetting.FILTERS_ADD_DEFAULT_PACKS]: {
      name: "Add default filters for compendiums",
      type: Boolean,
      default: false,
      scope: "world",
    },
    [ModuleSetting.FILTERS_ADD_DEFAULT_TYPE]: {
      name: "Add default filters for base types",
      type: Boolean,
      default: true,
      scope: "world",
    },
    [ModuleSetting.SEARCH_DENSITY]: {
      name: "Search Density",
      type: String,
      choices: {
        compact: "QUICKINSERT.SettingsSearchDensityValueCompact",
        comfortable: "QUICKINSERT.SettingsSearchDensityValueComfortable",
        spacious: "QUICKINSERT.SettingsSearchDensityValueSpacious",
      },
      default: "comfortable",
    },
    [ModuleSetting.ENHANCED_TOOLTIPS]: {
      name: "Enhanced tooltips",
      type: Boolean,
      default: true,
    },
    [ModuleSetting.SEARCH_ENGINE]: {
      name: "Search Engine",
      type: String,
      default: "fuzzysort",
      choices: {
        fuse: "QUICKINSERT.SettingsSearchEngineValueFuse",
        fuzzysort: "QUICKINSERT.SettingsSearchEngineValueFuzzysort",
      },
    },
    [ModuleSetting.QUICK_FILTER_EDIT]: {
      name: "QUICKINSERT.QuickFilterEdit",
      hint: "QUICKINSERT.QuickFilterEditHint",
      type: Boolean,
      default: false,
    },
    [ModuleSetting.REMEMBER_BROWSE_INPUT]: {
      name: "QUICKINSERT.SettingsRememberBrowseInput",
      hint: "QUICKINSERT.SettingsRememberBrowseInputHint",
      type: Boolean,
      default: true,
    },
  };
}

export function registerSettings(
  callbacks: {
    [setting: string]: (value?: unknown) => unknown;
  } = {}
): void {
  Object.entries(getModuleSettings()).forEach((entry) => {
    const [setting, item] = entry as [ModuleSetting, any];
    registerSetting(
      setting,
      (value) => {
        stores[setting]?.load();
        callbacks[setting]?.(value);
      },
      item
    );
  });
}
