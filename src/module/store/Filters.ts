export enum FilterType {
  Default,
  World,
  Client,
  Temporary,
}

export const FILTER_FOLDER_ROOT = "ROOT";
export const FILTER_COMPENDIUM_ALL = "ALL";

export interface SearchFilterConfig {
  compendiums: string[];
  folders: string[];
  documentTypes: string[];
  system?: Record<string, unknown>;
}

export interface SearchFilter {
  id: string;
  type: FilterType;
  tag: string; // used with @tag
  subTitle: string; // Title
  filterConfig: SearchFilterConfig;
  disabled?: boolean;
  role: ValueOf<typeof CONST.USER_ROLES>;
}

export interface WorldSavedFilters {
  filters: SearchFilter[];
}

export interface ClientSavedFilters {
  disabled: string[];
  filters: SearchFilter[];
}

export function cloneFilterConfig(
  original: SearchFilterConfig
): SearchFilterConfig {
  return {
    compendiums: [...original.compendiums],
    folders: [...original.folders],
    documentTypes: [...original.documentTypes],
    ...(original.system ? { system: { ...original.system } } : undefined),
  };
}
