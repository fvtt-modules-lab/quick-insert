import { writable, get, type Updater } from "svelte/store";
import { getSetting, setSetting } from "../settings";
import type { ClientSavedFilters, WorldSavedFilters } from "./Filters";
import type { IndexingDisabledSetting } from "./IndexingDisabledType";
import { ModuleSetting } from "./ModuleSettings";

function createStore<SettingType>(setting: ModuleSetting) {
  const store = writable<SettingType>();
  const { subscribe, set } = store;

  return {
    subscribe,
    set: (value: SettingType) => {
      if (value !== get(store)) {
        setSetting(setting, value);
      }

      return value;
    },
    update: (updater: Updater<SettingType>) => {
      const value = updater(get(store));
      setSetting(setting, value);
    },
    load: () => {
      const value = getSetting(setting) as SettingType;
      set(value);
    },
  };
}

export const stores = {
  [ModuleSetting.GM_ONLY]: createStore<boolean>(ModuleSetting.GM_ONLY),
  [ModuleSetting.FILTERS_SHEETS_ENABLED]: createStore<boolean>(
    ModuleSetting.FILTERS_SHEETS_ENABLED
  ),
  [ModuleSetting.AUTOMATIC_INDEXING]: createStore<number>(
    ModuleSetting.AUTOMATIC_INDEXING
  ),
  [ModuleSetting.SEARCH_BUTTON]: createStore<boolean>(
    ModuleSetting.SEARCH_BUTTON
  ),
  [ModuleSetting.ENABLE_GLOBAL_CONTEXT]: createStore<boolean>(
    ModuleSetting.ENABLE_GLOBAL_CONTEXT
  ),
  [ModuleSetting.INDEXING_DISABLED]: createStore<IndexingDisabledSetting>(
    ModuleSetting.INDEXING_DISABLED
  ),
  [ModuleSetting.FILTERS_CLIENT]: createStore<ClientSavedFilters>(
    ModuleSetting.FILTERS_CLIENT
  ),
  [ModuleSetting.FILTERS_WORLD]: createStore<WorldSavedFilters>(
    ModuleSetting.FILTERS_WORLD
  ),
  [ModuleSetting.FILTERS_ADD_DEFAULT_SUBTYPE]: createStore<boolean>(
    ModuleSetting.FILTERS_ADD_DEFAULT_SUBTYPE
  ),
  [ModuleSetting.FILTERS_ADD_DEFAULT_PACKS]: createStore<boolean>(
    ModuleSetting.FILTERS_ADD_DEFAULT_PACKS
  ),
  [ModuleSetting.FILTERS_ADD_DEFAULT_TYPE]: createStore<boolean>(
    ModuleSetting.FILTERS_ADD_DEFAULT_TYPE
  ),
  [ModuleSetting.DEFAULT_ACTION_MACRO]: createStore<string>(
    ModuleSetting.DEFAULT_ACTION_MACRO
  ),
  [ModuleSetting.DEFAULT_ACTION_ROLL_TABLE]: createStore<string>(
    ModuleSetting.DEFAULT_ACTION_ROLL_TABLE
  ),
  [ModuleSetting.DEFAULT_ACTION_SCENE]: createStore<string>(
    ModuleSetting.DEFAULT_ACTION_SCENE
  ),
  [ModuleSetting.SEARCH_TOOLTIPS]: createStore<string>(
    ModuleSetting.SEARCH_TOOLTIPS
  ),
  [ModuleSetting.EMBEDDED_INDEXING]: createStore<boolean>(
    ModuleSetting.EMBEDDED_INDEXING
  ),
  [ModuleSetting.SEARCH_DENSITY]: createStore<
    "compact" | "comfortable" | "spacious"
  >(ModuleSetting.SEARCH_DENSITY),
  [ModuleSetting.ENHANCED_TOOLTIPS]: createStore<boolean>(
    ModuleSetting.ENHANCED_TOOLTIPS
  ),
  [ModuleSetting.SEARCH_ENGINE]: createStore<"fuse" | "fuzzysort">(
    ModuleSetting.SEARCH_ENGINE
  ),
  [ModuleSetting.QUICK_FILTER_EDIT]: createStore<boolean>(
    ModuleSetting.QUICK_FILTER_EDIT
  ),
  [ModuleSetting.REMEMBER_BROWSE_INPUT]: createStore<boolean>(
    ModuleSetting.REMEMBER_BROWSE_INPUT
  ),
};

Hooks.on("ready", () => {
  Object.values(stores).forEach((store) => store.load());
});
