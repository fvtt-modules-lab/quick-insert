type Role = ValueOf<typeof CONST.USER_ROLES>;
export interface IndexingDisabledSetting {
  entities: {
    [key: string]: Role[];
  };
  packs: {
    [key: string]: Role[];
  };
  directory?: {
    [key: string]: Role[];
  };
}
