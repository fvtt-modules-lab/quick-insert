import { derived } from "svelte/store";
import { IndexedDocumentTypes, packEnabled } from "../searchLib";
import { loc, mloc } from "../utils";
import { ModuleSetting } from "./ModuleSettings";
import { stores } from "./SettingsStore";

// Generate view data from IndexingDisabledSetting
const enabledRoles = (
  disabled: number[] | null | undefined
): { [key: number]: boolean } => {
  return [1, 2, 3, 4].reduce(function (map, role) {
    map[role] = !disabled?.includes(role);
    return map;
  }, {} as { [key: number]: boolean });
};

function createDisabled() {
  const { subscribe, update } = stores[ModuleSetting.INDEXING_DISABLED];

  return {
    subscribe,
    toggleRole: (
      type: "entities" | "packs" | "directory",
      id: string,
      role: ValueOf<typeof CONST.USER_ROLES>,
      disabled: boolean
    ) =>
      update((value) => {
        if (!value[type]) value[type] = { root: [] };

        let roles = value[type][id];
        if (!roles) {
          value[type][id] = roles = [];
        }
        const roleIndex = roles.indexOf(role);
        if (disabled && roleIndex === -1) {
          roles.push(role);
        } else if (!disabled && roleIndex !== -1) {
          roles.splice(roleIndex, 1);
        }
        if (roles.length === 0) {
          delete value[type][id];
        }
        return value;
      }),
    toggleAll: (
      type: "entities" | "packs" | "directory",
      id: string,
      disabled: boolean
    ) =>
      update((value) => {
        if (!value[type]) return value;

        if (disabled) {
          value[type][id] = [1, 2, 3, 4];
        } else {
          delete value[type][id];
        }
        return value;
      }),
  };
}

export const disabled = createDisabled();

// Derived read-only stores used for views

export const documents = derived(disabled, ($disabled) => {
  return IndexedDocumentTypes.map((type) => ({
    type: "entities" as const,
    id: type as string,
    title: loc(`DOCUMENT.${type}`),
    enabled: enabledRoles($disabled.entities[type]),
  }));
});

export const packs = derived(disabled, ($disabled) => {
  return game.packs
    ? [...game.packs.contents].map((pack) => ({
        type: "packs" as const,
        id: pack.collection,
        title: pack.title,
        documentType: pack.documentName,
        subTitle: `${loc(`DOCUMENT.${pack.documentName}`)} (${
          pack.metadata.packageName
        })`,
        enabled: enabledRoles($disabled.packs[pack.collection]),
      }))
    : [];
});

export const directory = derived(disabled, ($disabled) => {
  return {
    type: "directory" as const,
    id: "root",
    title: mloc("FilterEditorDirectory"),
    enabled: enabledRoles($disabled.directory?.["root"]),
  };
});

export const enabledDocumentTypes = derived(
  [stores[ModuleSetting.INDEXING_DISABLED]],
  ([$disabled]) => {
    const role = game.user?.role || 4;
    return IndexedDocumentTypes.filter(
      (t) => !$disabled?.entities?.[t]?.includes(role)
    );
  }
);

export const enabledPacks = derived(
  [stores[ModuleSetting.INDEXING_DISABLED]],
  () => (game.packs ? game.packs.filter(packEnabled) : [])
);
