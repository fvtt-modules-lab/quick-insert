import { getSetting, setSetting } from "module/settings";
import {
  FILTER_COMPENDIUM_ALL,
  FILTER_FOLDER_ROOT,
  FilterType,
  type ClientSavedFilters,
  type SearchFilter,
  type SearchFilterConfig,
  type WorldSavedFilters,
} from "./Filters";
import { ModuleSetting } from "./ModuleSettings";

// Convert V2 config to V3 config
function v2ToV3(config: SearchFilterConfig) {
  if (!("entities" in config)) {
    return false;
  }

  // Entities -> documentTypes
  if (typeof config.entities !== "string") {
    const entities = config.entities as string[];
    delete config.entities;
    config.documentTypes = entities;
  } else {
    delete config.entities;
    config.documentTypes = [];
  }

  // Compendiums
  if (typeof config.compendiums === "string" && config.compendiums === "any") {
    config.compendiums = [FILTER_COMPENDIUM_ALL];
  }

  // Folders
  if (typeof config.folders === "string" && config.folders === "any") {
    config.folders = [FILTER_FOLDER_ROOT];
  }

  return true;
}

function migrateConfig(config: SearchFilterConfig) {
  return v2ToV3(config);
}

function migrateFilter(filter: SearchFilter) {
  let migrated = false;

  if (filter.role === undefined) {
    filter.role =
      filter.type === FilterType.Client
        ? CONST.USER_ROLES.PLAYER
        : CONST.USER_ROLES.GAMEMASTER;
    migrated = true;
  }

  migrated = migrateConfig(filter.filterConfig) || migrated;

  if (migrated) {
    console.log(
      `Quick Insert | migrated custom filter to V3: @${filter.tag}`,
      filter
    );
  }

  return migrated;
}

export function migrateFilters() {
  let migratedClient = false;
  const clientFilters = getSetting(
    ModuleSetting.FILTERS_CLIENT
  ) as ClientSavedFilters;
  if (clientFilters?.filters?.length) {
    clientFilters.filters.forEach(
      (filter) => (migratedClient = migrateFilter(filter) || migratedClient)
    );
    if (migratedClient) setSetting(ModuleSetting.FILTERS_CLIENT, clientFilters);
  }

  if (game.user?.isGM) {
    let migratedWorld = false;

    const worldFilters = getSetting(
      ModuleSetting.FILTERS_WORLD
    ) as WorldSavedFilters;
    if (worldFilters?.filters?.length) {
      worldFilters.filters.forEach(
        (filter) => (migratedWorld = migrateFilter(filter) || migratedWorld)
      );
    }
    if (migratedWorld) setSetting(ModuleSetting.FILTERS_WORLD, worldFilters);
  }
}
