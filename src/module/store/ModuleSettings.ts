export const SAVE_SETTINGS_REVISION = 1;

export enum ModuleKeyBinds {
  TOGGLE_OPEN = "toggleOpen",
  OPEN_SETTINGS = "openSettings",
  // Unimplemented
  TOGGLE_FILTER = "toggleFilter",
  TOGGLE_QUICK_FILTER = "toggleQuickFilter",
}

export enum ModuleSetting {
  // Dead settings, do not reuse!
  // QUICKOPEN = "quickOpen",
  // INDEX_TIMEOUT = "indexTimeout",
  // FILTERS_INCLUDE_DEFAULTS = "filtersIncludeDefaults",

  ENABLE_GLOBAL_CONTEXT = "enableGlobalContext",
  INDEXING_DISABLED = "indexingDisabled",
  FILTERS_CLIENT = "filtersClient",
  FILTERS_WORLD = "filtersWorld",
  FILTERS_ADD_DEFAULT_SUBTYPE = "filtersAddDefaultSuptype",
  FILTERS_ADD_DEFAULT_PACKS = "filtersAddDefaultPack",
  FILTERS_ADD_DEFAULT_TYPE = "filtersAddDefaultType",
  FILTERS_SHEETS_ENABLED = "filtersSheetsEnabled",
  GM_ONLY = "gmOnly",
  AUTOMATIC_INDEXING = "automaticIndexing",
  SEARCH_BUTTON = "searchButton",
  DEFAULT_ACTION_SCENE = "defaultActionScene",
  DEFAULT_ACTION_ROLL_TABLE = "defaultActionRollTable",
  DEFAULT_ACTION_MACRO = "defaultActionMacro",
  SEARCH_TOOLTIPS = "searchTooltips",
  EMBEDDED_INDEXING = "embeddedIndexing",
  SEARCH_DENSITY = "searchDensity",
  ENHANCED_TOOLTIPS = "enhancedTooltips",
  SEARCH_ENGINE = "searchEngine",
  QUICK_FILTER_EDIT = "quickFilterEdit",
  REMEMBER_BROWSE_INPUT = "rememberBrowseInput",
}
