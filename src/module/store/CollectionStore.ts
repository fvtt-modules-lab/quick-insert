import { readable, type Readable } from "svelte/store";

function filterDisplayed(contents: unknown[]) {
  return contents.filter((i) => (i as any).displayed);
}

export function createCollectionStore<
  Collection extends { contents: unknown[]; documentName: string }
>(collection: Collection) {
  return readable<Collection["contents"][number][]>([], function start(set) {
    function onCreate() {
      set(filterDisplayed(collection.contents));
    }
    function onDelete() {
      set(filterDisplayed(collection.contents));
    }

    const type = collection.documentName;

    Hooks.on(`create${type}`, onCreate);
    Hooks.on(`delete${type}`, onDelete);

    set(filterDisplayed(collection.contents));

    return function stop() {
      Hooks.off(`create${type}`, onCreate);
      Hooks.off(`delete${type}`, onDelete);
    };
  });
}

export const collectionStores: {
  folders?: Readable<Folder[]>;
} = {};

Hooks.on("ready", () => {
  if (game.folders) {
    collectionStores.folders = createCollectionStore(game.folders);
  }
});
