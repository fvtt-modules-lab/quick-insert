import { derived, get } from "svelte/store";
import { enabledDocumentTypes } from "./IndexingDisabledStore";
import { DocumentMeta, DocumentType, packEnabled } from "../searchLib";
import { FilterType, type SearchFilter } from "./Filters";
import { ModuleSetting } from "./ModuleSettings";
import { stores } from "./SettingsStore";
import { getSubTypes, typeLabel } from "module/typeUtils";

function createDefaultStore() {
  return derived(
    [
      enabledDocumentTypes,
      stores[ModuleSetting.FILTERS_ADD_DEFAULT_PACKS],
      stores[ModuleSetting.FILTERS_ADD_DEFAULT_TYPE],
      stores[ModuleSetting.FILTERS_ADD_DEFAULT_SUBTYPE],
    ],
    ([$enabledTypes, enablePacks, enableTypes, enableSubtypes]) => {
      let filters: SearchFilter[] = [];

      if (enableTypes) {
        const typeFilters: SearchFilter[] = $enabledTypes.map((type) => {
          const metadata = DocumentMeta[type];
          return {
            id: metadata.collection,
            type: FilterType.Default,
            tag: metadata.collection,
            subTitle: `${game.i18n.localize(metadata.label)}`,
            filterConfig: {
              documentTypes: [metadata.name],
              folders: [],
              compendiums: [],
            },
            role: CONST.USER_ROLES.PLAYER,
          };
        });
        filters = filters.concat(typeFilters);
      }
      if (enablePacks && game.packs) {
        const packFilters: SearchFilter[] = game.packs
          .filter(packEnabled)
          .map((pack) => {
            return {
              id: pack.collection,
              type: FilterType.Default,
              tag: pack.collection,
              subTitle: pack.metadata.label,
              filterConfig: {
                documentTypes: [],
                folders: [],
                compendiums: [pack.collection],
              },
              role: CONST.USER_ROLES.PLAYER,
            };
          });
        filters = filters.concat(packFilters);
      }

      if (enableSubtypes) {
        const subTypes = $enabledTypes
          .filter(
            (type) => type === DocumentType.ITEM || type === DocumentType.ACTOR
          )
          .map(getSubTypes)
          .flat();

        const subTypeFilters: SearchFilter[] = subTypes.map((subType) => {
          return {
            id: subType,
            type: FilterType.Default,
            tag: subType.split(":")[1],
            subTitle: typeLabel(subType),
            filterConfig: {
              documentTypes: [subType],
              folders: [],
              compendiums: [],
            },
            role: CONST.USER_ROLES.PLAYER,
          };
        });

        filters = filters.concat(subTypeFilters);
      }

      return filters;
    }
  );
}

const defaultFilters = createDefaultStore();

function createCompoundStore() {
  return derived(
    [
      defaultFilters,
      stores[ModuleSetting.FILTERS_CLIENT],
      stores[ModuleSetting.FILTERS_WORLD],
    ],
    ([$default, $client, $world]) => {
      const filters: Record<string, SearchFilter> = {};

      $default.forEach((filter) => (filters[filter.id] = filter));
      $client.filters.forEach((filter) => (filters[filter.id] = filter));
      $world.filters.forEach((filter) => (filters[filter.id] = filter));
      Object.keys(filters).forEach((id) => (filters[id].disabled = false));
      $client.disabled.forEach((key) => {
        if (key in filters) filters[key].disabled = true;
      });

      return filters;
    }
  );
}

export const filterStore = createCompoundStore();
export const visibleFilters = derived(filterStore, (filters) => {
  return Object.values(filters).filter(
    (filter) => filter.role <= (game.user?.role || 0) && !filter.disabled
  );
});

export function toggleFilter(id: string) {
  stores[ModuleSetting.FILTERS_CLIENT].update((v) => {
    const disabled = v.disabled.includes(id)
      ? v.disabled.filter((d) => d !== id)
      : [...v.disabled, id];

    return { ...v, disabled };
  });
}

export function deleteFilter(id: string) {
  const store = get(filterStore);
  if (store[id].type === FilterType.Client) {
    stores[ModuleSetting.FILTERS_CLIENT].update((v) => {
      return { ...v, filters: v.filters.filter((f) => f.id !== id) };
    });
  } else if (store[id].type === FilterType.World) {
    stores[ModuleSetting.FILTERS_WORLD].update((v) => {
      return { ...v, filters: v.filters.filter((f) => f.id !== id) };
    });
  }
}

export function addFilter(filter: SearchFilter) {
  if (filter.type === FilterType.Client) {
    stores[ModuleSetting.FILTERS_CLIENT].update((v) => {
      return { ...v, filters: [...v.filters, filter] };
    });
  } else if (filter.type === FilterType.World) {
    stores[ModuleSetting.FILTERS_WORLD].update((v) => {
      return { ...v, filters: [...v.filters, filter] };
    });
  }
}

export function updateFilter(filter: SearchFilter) {
  if (filter.type === FilterType.Client) {
    stores[ModuleSetting.FILTERS_CLIENT].update((v) => {
      const i = v.filters.findIndex((f) => f.id === filter.id);
      if (i !== -1) {
        v.filters[i] = filter;
      }
      return { ...v };
    });
  } else if (filter.type === FilterType.World) {
    stores[ModuleSetting.FILTERS_WORLD].update((v) => {
      const i = v.filters.findIndex((f) => f.id === filter.id);
      if (i !== -1) {
        v.filters[i] = filter;
      }
      return { ...v };
    });
  }
}
