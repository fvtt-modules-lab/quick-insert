import { mount, unmount } from "svelte";
import { mloc } from "module/utils";
import { openAboutApp } from "app/about/AboutApp";
import SettingsPage from "app/settings/SettingsPage.svelte";

const AppV2 = foundry.applications.api.ApplicationV2;

export class SettingsApp extends AppV2 {
  static DEFAULT_OPTIONS = {
    id: "qi-settings-app",
    window: {
      title: "QUICKINSERT.SettingsMenuLabel",
      contentTag: "section",
      frame: true,
      positioned: true,
      minimizable: true,
      resizable: true,
    },
    position: {
      width: 750,
      height: 600,
    },
  };

  #unmount?: Record<string, any>;

  constructor(options = {}) {
    super(options);
  }

  protected _onClose(): void {
    if (this.#unmount) {
      unmount(this.#unmount, { outro: false });
    }
  }

  async _renderFrame(options: any) {
    const frame = await super._renderFrame(options);

    const settingsLabel = mloc("AboutTitle");
    const openAboutPage = `<button type="button" class="header-control icon fa-solid fa-circle-info" data-action="openAboutPage"
                          data-tooltip="${settingsLabel}" aria-label="${settingsLabel}"></button>`;
    this.window?.close?.insertAdjacentHTML("beforebegin", openAboutPage);
    return frame;
  }

  override _onClickAction(event: PointerEvent, target: HTMLElement) {
    const action = target.dataset.action;
    if (action === "openAboutPage") {
      openAboutApp();
    }
  }

  override async _renderHTML(): Promise<string> {
    return "";
  }

  override _replaceHTML(result: string, content: HTMLElement) {
    this.#unmount = mount(SettingsPage, { target: content });
  }
}
