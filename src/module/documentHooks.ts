import type { QuickInsert } from "./core";
import {
  directoryEnabled,
  DocumentType,
  enabledDocumentTypes,
  enabledEmbeddedDocumentTypes,
  packEnabled,
  searchItemFromDocument,
} from "./searchLib";

function checkIndexed(document: AnyDocument, embedded = false) {
  if (!document.visible) return false;

  // Check embedded state
  if ((embedded && !document.parent) || (!embedded && document.parent)) {
    return false;
  }

  // Check enabled types
  if (document.parent) {
    if (!enabledEmbeddedDocumentTypes().includes(document.documentName))
      return false;
  } else {
    if (!enabledDocumentTypes().includes(document.documentName as DocumentType))
      return false;
  }

  // Check disabled packs
  return !(
    document.pack &&
    document.compendium &&
    !packEnabled(document.compendium)
  );
}

export function setupDocumentHooks(quickInsert: typeof QuickInsert) {
  enabledDocumentTypes().forEach((type) => {
    Hooks.on(`create${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;

      if (document.parent || !checkIndexed(document)) return;
      quickInsert.searchLib?.addItem(searchItemFromDocument(document));
    });
    Hooks.on(`update${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;

      if (document.parent) return;
      if (!checkIndexed(document)) {
        quickInsert.searchLib?.removeItem(document.uuid);
        return;
      }
      quickInsert.searchLib?.replaceItem(searchItemFromDocument(document));
    });
    Hooks.on(`delete${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;

      if (document.parent || !checkIndexed(document)) return;
      quickInsert.searchLib?.removeItem(document.uuid);
    });
  });

  enabledEmbeddedDocumentTypes().forEach((type) => {
    Hooks.on(`create${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;

      if (!document.parent || !checkIndexed(document, true)) return;

      const item = searchItemFromDocument(document);
      quickInsert.searchLib?.addItem(item);
    });

    Hooks.on(`update${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;
      if (!document.parent) return;

      if (!checkIndexed(document, true)) {
        quickInsert.searchLib?.removeItem(document.uuid);
        return;
      }
      const item = searchItemFromDocument(document);
      quickInsert.searchLib?.replaceItem(item);
    });
    Hooks.on(`delete${type}`, (document: AnyDocument) => {
      if (!directoryEnabled()) return;
      if (!document.parent || !checkIndexed(document, true)) return;

      quickInsert.searchLib?.removeItem(document.uuid);
    });
  });
}
