const MODULE_NAME = "quick-insert";
export const SAVE_SETTINGS_REVISION = 1;

type Callback = (value?: unknown) => void;

export function registerSetting(
  setting: string,
  callback: Callback,
  { ...options }: Record<string, unknown>
): void {
  //@ts-expect-error Settings system needs alignment with new types
  game.settings.register(MODULE_NAME, setting, {
    config: false,
    scope: "client",
    ...options,
    onChange: callback || undefined,
  });
}

export function getSetting(setting: string): any {
  //@ts-expect-error Settings system needs alignment with new types
  return game.settings.get(MODULE_NAME, setting);
}

export function setSetting(setting: string, value: unknown): Promise<unknown> {
  //@ts-expect-error Settings system needs alignment with new types
  return game.settings.set(MODULE_NAME, setting, value);
}

export function registerMenu({
  menu,
  ...options
}: { menu: string } & ClientSettings.SettingSubmenuConfig): void {
  game.settings.registerMenu(MODULE_NAME, menu, options as any);
}
