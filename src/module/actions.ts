import {
  CompendiumSearchItem,
  EntitySearchItem,
  DocumentType,
  IndexedDocumentTypes,
  documentIcons,
  getCollectionFromType,
  SearchItem,
} from "./searchLib";
import { getSetting } from "./settings";
import { ModuleSetting } from "./store/ModuleSettings";

export interface DocumentAction {
  id: string;
  icon: string;
  title: string;
  hint?: string;
}

export const DOCUMENTACTIONS: Record<string, (item: SearchItem) => any> = {
  show: (item) => item.show(),
  roll: (item) => item.get().then((d) => d.draw()),
  viewScene: (item) => item.get().then((d) => d.view()),
  activateScene: (item) =>
    item.get().then((d) => {
      if (game.user?.isGM) {
        d.activate();
      }
    }),
  execute: (item) => item.get().then((d) => d.execute()),

  insert: (item) => item,
  rollInsert: (item) =>
    item.get().then(async (d): Promise<string | SearchItem | undefined> => {
      const roll = await d.roll();
      for (const data of roll.results) {
        if (!data.documentId) {
          return data.text;
        }

        if (data.documentCollection.includes(".")) {
          const pack = game.packs.get(data.documentCollection);
          if (!pack) return data.text;

          const indexItem = game.packs
            .get(data.documentCollection)
            ?.index.find((i) => i._id === data.documentId);
          return indexItem
            ? new CompendiumSearchItem(pack, indexItem)
            : data.text;
        } else {
          const entity = getCollectionFromType(data.documentCollection).get(
            data.documentId
          );
          return entity ? new EntitySearchItem(entity as any) : data.text;
        }
      }
    }),
};

let memoizedBrowseDocumentActions:
  | Record<string, DocumentAction[]>
  | undefined = undefined;
function getBrowseDocumentActions(): Record<string, DocumentAction[]> {
  if (memoizedBrowseDocumentActions) {
    return memoizedBrowseDocumentActions;
  }

  const actions: Record<string, DocumentAction[]> = {
    [DocumentType.SCENE]: [
      {
        id: "activateScene",
        icon: "fas fa-bullseye",
        title: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.Activate"
          : "SCENES.Activate",
      },
      {
        id: "viewScene",
        icon: "fas fa-eye",
        title: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.View"
          : "SCENES.View",
      },
      {
        id: "show",
        icon: "fas fa-cogs",
        title: foundry.utils.isNewerVersion(game.version, "13")
          ? "SCENE.Configure"
          : "SCENES.Configure",
      },
    ],
    [DocumentType.ROLLTABLE]: [
      {
        id: "roll",
        icon: "fas fa-dice-d20",
        title: foundry.utils.isNewerVersion(game.version, "13")
          ? "TABLE.ACTIONS.DrawResult"
          : "TABLE.Roll",
      },
      {
        id: "show",
        icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
        title: "QUICKINSERT.ActionEdit",
      },
    ],
    [DocumentType.MACRO]: [
      {
        id: "execute",
        icon: "fas fa-play",
        title: "QUICKINSERT.ActionExecute",
      },
      {
        id: "show",
        icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
        title: "QUICKINSERT.ActionEdit",
      },
    ],
  };
  IndexedDocumentTypes.forEach((type) => {
    if (type in actions) return;
    actions[type] = [
      {
        id: "show",
        icon: `fas ${documentIcons[type]}`,
        title: "QUICKINSERT.ActionShow",
      },
    ];
  });
  memoizedBrowseDocumentActions = actions;
  return actions;
}

// Same for all inserts
const insertAction = {
  id: "insert",
  icon: `fas fa-plus`,
  title: "Insert",
};

let memoizedInsertDocumentActions:
  | Record<string, DocumentAction[]>
  | undefined = undefined;
function getInsertDocumentActions(): Record<string, DocumentAction[]> {
  if (memoizedInsertDocumentActions) {
    return memoizedInsertDocumentActions;
  }
  const actions: Record<string, DocumentAction[]> = {
    [DocumentType.SCENE]: [
      {
        id: "show",
        icon: "fas fa-cogs",
        title: "Configure",
      },
    ],
    [DocumentType.ROLLTABLE]: [
      {
        id: "rollInsert",
        icon: "fas fa-play",
        title: "Roll and Insert",
      },
      {
        id: "show",
        icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
        title: "Show",
      },
    ],
  };

  // Add others
  IndexedDocumentTypes.forEach((type) => {
    if (!actions[type]) {
      // If nothing else, add "Show"
      actions[type] = [
        {
          id: "show",
          icon: `fas ${documentIcons[type]}`,
          title: "Show",
        },
      ];
    }
    actions[type].push(insertAction);
  });
  memoizedInsertDocumentActions = actions;
  return actions;
}

export function getActions(
  type: DocumentType,
  isInsertContext: boolean
): DocumentAction[] {
  return isInsertContext
    ? getInsertDocumentActions()[type]
    : getBrowseDocumentActions()[type];
}

export function getDefaultActions(): Record<string, string | undefined> {
  return {
    [DocumentType.SCENE]: getSetting(ModuleSetting.DEFAULT_ACTION_SCENE),
    [DocumentType.ROLLTABLE]: getSetting(
      ModuleSetting.DEFAULT_ACTION_ROLL_TABLE
    ),
    [DocumentType.MACRO]: getSetting(ModuleSetting.DEFAULT_ACTION_MACRO),
  };
}

export function defaultAction(
  type: DocumentType,
  isInsertContext: boolean
): string {
  if (!isInsertContext) {
    switch (type) {
      case DocumentType.SCENE:
        return getSetting(ModuleSetting.DEFAULT_ACTION_SCENE);
      case DocumentType.ROLLTABLE:
        return getSetting(ModuleSetting.DEFAULT_ACTION_ROLL_TABLE);
      case DocumentType.MACRO:
        return getSetting(ModuleSetting.DEFAULT_ACTION_MACRO);
      default:
        break;
    }
  }

  const actions = getActions(type, isInsertContext);
  return actions[actions.length - 1].id;
}
