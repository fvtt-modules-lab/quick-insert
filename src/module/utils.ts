const namespace = "QUICKINSERT";

// Module-localized
export const mloc = (
  name: string,
  replacements?: { [key: string]: string }
): string => {
  if (!name) return "";
  if (name.includes(".")) {
    // TODO: Remove this when sure
    console.error("don't use mloc", name);
  }

  if (replacements) {
    return game.i18n.format(`${namespace}.${name}`, replacements);
  }
  return game.i18n.localize(`${namespace}.${name}`);
};

// Global-localized
export const loc = (
  name: string,
  replacements?: { [key: string]: string }
): string => {
  if (!name) return "";

  if (replacements) {
    return game.i18n.format(name, replacements);
  }
  return game.i18n.localize(name);
};

// Type utils
export type TextInputElement = HTMLInputElement | HTMLTextAreaElement;
export function isTextInputElement(
  element: Element
): element is TextInputElement {
  return (
    element.tagName == "TEXTAREA" ||
    (element.tagName == "INPUT" && (element as HTMLInputElement).type == "text")
  );
}

// General utils

const ALPHA = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
export function randomId(idLength = 10): string {
  const values = new Uint8Array(idLength);
  window.crypto.getRandomValues(values);
  return String.fromCharCode(
    ...values.map((x) => ALPHA.charCodeAt(x % ALPHA.length))
  );
}

// Some black magic from the internet,
// places caret at end of contenteditable
export function placeCaretAtEnd(el?: HTMLElement): void {
  if (!el) return;
  el.focus();
  const range = document.createRange();
  range.selectNodeContents(el);
  range.collapse(false);
  const sel = window.getSelection();
  sel?.removeAllRanges();
  sel?.addRange(range);
}

// Simple utility function for async waiting
// Nicer to await waitFor(100) than nesting setTimeout callback hell
export function resolveAfter(msec: number): Promise<void> {
  return new Promise((res) => setTimeout(res, msec));
}

export class TimeoutError extends Error {
  constructor(timeoutMsec: number) {
    super(`did not complete within ${timeoutMsec}ms`);
  }
}

export function withDeadline<T>(
  p: Promise<T>,
  timeoutMsec: number
): Promise<T> {
  return Promise.race([
    p,
    new Promise<T>((res, rej) =>
      setTimeout(() => rej(new TimeoutError(timeoutMsec)), timeoutMsec)
    ),
  ]);
}

export function permissionListEq(a: number[], b: readonly number[]): boolean {
  return a.length === b.length && [...a].every((value) => b.includes(value));
}

// Match keybinds even if it's in input fields or with explicit context
export function customKeybindHandler(
  evt: KeyboardEvent,
  context?: unknown
): void {
  if (evt.isComposing || (!evt.key && !evt.code)) {
    return;
  }

  if (!context && !game.keyboard?.hasFocus) return;

  const ctx = KeyboardManager.getKeyboardEventContext(evt, false);
  if (
    (ctx.event.target as HTMLElement | null)?.dataset?.engine === "prosemirror"
  ) {
    return;
  }

  if (context) {
    ctx._quick_insert_extra = { context };
  }

  // ProseMirror handles keys itself!
  if (document.activeElement?.classList.contains("ProseMirror")) {
    return;
  }

  const actions = KeyboardManager._getMatchingActions(ctx)
    .map((action: { action: string }) =>
      game.keybindings.actions.get(action.action)
    )
    .filter((action) => action?.textInput);

  if (!actions.length) return;
  let handled = false;
  for (const action of actions) {
    //@ts-expect-error using protected, I know
    handled = KeyboardManager._executeKeybind(action, ctx);
    if (handled) break;
  }

  if (handled) {
    evt.preventDefault();
    evt.stopPropagation();
  }
}
