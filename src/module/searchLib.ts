import Fuse from "fuse.js";
import fuzzysort from "fuzzysort-esm";
import { getSetting } from "./settings";
import { ModuleSetting } from "./store/ModuleSettings";
import { loc, TimeoutError, withDeadline } from "./utils";

import type { IndexingDisabledSetting } from "./store/IndexingDisabledType";
import { systemFields } from "./systemFields";

export enum DocumentType {
  ACTOR = "Actor",
  ITEM = "Item",
  JOURNALENTRY = "JournalEntry",
  MACRO = "Macro",
  ROLLTABLE = "RollTable",
  SCENE = "Scene",
  PLAYLIST = "Playlist",
  CARDS = "Cards",
  ADVENTURE = "Adventure",
}

export const IndexedDocumentTypes = [
  DocumentType.ACTOR,
  DocumentType.ITEM,
  DocumentType.JOURNALENTRY,
  DocumentType.MACRO,
  DocumentType.ROLLTABLE,
  DocumentType.SCENE,
];

export const EmbeddedDocumentTypes: Record<string, string> = {
  [DocumentType.JOURNALENTRY]: "JournalEntryPage",
};

export const EmbeddedDocumentCollections = {
  [DocumentType.JOURNALENTRY]: "pages",
};

export const DocumentMeta = {
  [DocumentType.ACTOR]: CONFIG.Actor.documentClass.metadata,
  [DocumentType.ITEM]: CONFIG.Item.documentClass.metadata,
  [DocumentType.JOURNALENTRY]: CONFIG.JournalEntry.documentClass.metadata,
  [DocumentType.MACRO]: CONFIG.Macro.documentClass.metadata,
  [DocumentType.ROLLTABLE]: CONFIG.RollTable.documentClass.metadata,
  [DocumentType.SCENE]: CONFIG.Scene.documentClass.metadata,
  [DocumentType.PLAYLIST]: CONFIG.Playlist.documentClass.metadata,
  [DocumentType.CARDS]: CONFIG.Cards.documentClass.metadata,
  [DocumentType.ADVENTURE]: CONFIG.Adventure.documentClass.metadata,
};

export const documentIcons = {
  [DocumentType.ACTOR]: "fa-user",
  [DocumentType.ITEM]: "fa-suitcase",
  [DocumentType.JOURNALENTRY]: "fa-book-open",
  [DocumentType.MACRO]: "fa-terminal",
  [DocumentType.ROLLTABLE]: "fa-th-list",
  [DocumentType.SCENE]: "fa-map",
  [DocumentType.PLAYLIST]: "fa-music",
  [DocumentType.CARDS]: "fa-id-badge",
  [DocumentType.ADVENTURE]: "fa-globe-asia",
};

interface IndexItem {
  _id: string;
  name?: string;
  img?: string | null;
  type?: string;
}

interface JournalIndexItem extends IndexItem {
  pages?: { _id: string; name: string }[];
}

function extractEmbeddedIndex(
  item: JournalIndexItem | IndexItem,
  pack?: CompendiumPack
): EmbeddedCompendiumSearchItem[] | undefined {
  if (!("pages" in item)) return;

  if (pack && item.pages?.length && item.pages[0]._id) {
    return item.pages.map(
      (page, i) =>
        new EmbeddedCompendiumSearchItem(pack, {
          _id: page._id,
          parentName: item.name,
          embeddedName: page.name,
          parentId: item._id,
          type: "JournalEntryPage",
          tagline: `Pg. ${i} - ${pack?.metadata?.label || pack.title}`,
        })
    );
  }
}

function showDocument(doc?: AnyDocument | null) {
  if (!doc) {
    return;
  }

  if (
    doc.documentName === "JournalEntry" ||
    doc.documentName === "JournalEntryPage"
  ) {
    //@ts-expect-error This is good enough for now
    doc._onClickDocumentLink({ currentTarget: { dataset: {} } });
  } else {
    doc.sheet?.render(true);
  }
}

export function getCollectionFromType(type: DocumentType): GenericCollection {
  return (CONFIG[type] as typeof CONFIG.Actor).collection.instance;
}

type CompendiumPack = CompendiumCollection<CompendiumCollection.Metadata>;

const ignoredFolderNames: Record<string, boolean> = { _fql_quests: true };

export function directoryEnabled(): boolean {
  const disabled = getSetting(ModuleSetting.INDEXING_DISABLED) as
    | IndexingDisabledSetting
    | undefined;

  const role = game.user?.role;
  if (!role) return false;

  return !disabled?.directory?.["root"]?.includes(role);
}

export function enabledDocumentTypes(): DocumentType[] {
  const disabled = getSetting(ModuleSetting.INDEXING_DISABLED) as
    | IndexingDisabledSetting
    | undefined;
  const role = game.user?.role;
  if (!role) return [];

  return IndexedDocumentTypes.filter(
    (t) => !disabled?.entities?.[t]?.includes(role)
  );
}

export function enabledEmbeddedDocumentTypes(): string[] {
  if (
    enabledDocumentTypes().includes(DocumentType.JOURNALENTRY) &&
    getSetting(ModuleSetting.EMBEDDED_INDEXING)
  ) {
    return [EmbeddedDocumentTypes[DocumentType.JOURNALENTRY]];
  }
  return [];
}

export function packEnabled(pack: CompendiumPack): boolean {
  const disabled = getSetting(ModuleSetting.INDEXING_DISABLED) as
    | IndexingDisabledSetting
    | undefined;
  // Pack entity type enabled?
  const role = game.user?.role;

  if (role) {
    if (disabled?.entities?.[pack.metadata.type]?.includes(role)) {
      return false;
    }

    // Pack enabled?
    if (disabled?.packs?.[pack.collection]?.includes(role)) {
      return false;
    }

    // Pack entity type indexed?
    if (!IndexedDocumentTypes.includes(pack.metadata.type as DocumentType)) {
      return false;
    }
  }

  // Not hidden?
  return Boolean(pack.visible || game.user?.isGM);
}

export function getDirectoryName(type: DocumentType): string {
  const documentLabel = DocumentMeta[type].labelPlural;
  return loc("SIDEBAR.DirectoryTitle", {
    type: documentLabel ? loc(documentLabel) : type,
  });
}

export function getSubTypeName(documentType: DocumentType, subType: string) {
  //@ts-expect-error typeLabels not in types yet
  return loc(CONFIG[documentType].typeLabels?.[subType]);
}

export type SearchResultPredicate = (item: SearchResult) => boolean;

interface SearchItemData {
  id: string;
  uuid: string;
  name: string;
  documentType: DocumentType;
  subType?: string;
  img?: string | null;
  system?: Record<string, unknown>;
}

export abstract class SearchItem {
  __source = "quick-insert";
  id: string;
  uuid: string;
  name: string;
  documentType: DocumentType;
  subType?: string;
  img?: string | null;
  system?: Record<string, unknown>;

  constructor(data: SearchItemData) {
    this.id = data.id;
    this.uuid = data.uuid;
    this.name = data.name;
    this.documentType = data.documentType;
    this.subType = data.subType;
    this.img = data.img;
    this.system = data.system;
  }

  // Get the drag data for drag operations
  get dragData(): Record<string, string> {
    return {};
  }
  // Get the html for an icon that represents the item
  get icon(): string {
    return "";
  }
  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return "";
  }
  // Reference the entity in a script
  get script(): string {
    return "";
  }
  // Short tagline that explains where/what this is
  get tagline(): string {
    return "";
  }

  // Additional details for result tooltips
  get tooltip(): string {
    const type = this.subType
      ? getSubTypeName(this.documentType, this.subType)
      : loc(DocumentMeta[this.documentType].label);
    return `${type} - ${this.tagline}`;
  }

  // Show the sheet or equivalent of this search result
  async show(): Promise<void> {
    return;
  }

  // Fetch the original object (or null if no longer available).
  // NEVER call as part of indexing or filtering.
  // It can be slow and most calls will cause a request to the database!
  // Call it once a decision is made, do not call for every SearchItem!
  async get(): Promise<any> {
    return null;
  }
}

export class EntitySearchItem extends SearchItem {
  folder?: { id: string; name: string };

  static fromDocuments(
    documents: AnyDocument[]
  ): (EntitySearchItem | EmbeddedEntitySearchItem)[] {
    return documents
      .filter((e) => {
        return (
          e.visible && !(e.folder?.name && ignoredFolderNames[e.folder.name])
        );
      })
      .map((doc) => {
        let embedded: EmbeddedEntitySearchItem[] | undefined;
        if (
          EmbeddedDocumentTypes[doc.documentName] &&
          enabledEmbeddedDocumentTypes().includes(
            EmbeddedDocumentTypes[doc.documentName]
          )
        ) {
          const collection: AnyDocument[] =
            //@ts-expect-error can't type this right now
            doc[EmbeddedDocumentCollections[doc.documentName]];
          embedded = collection.map(EmbeddedEntitySearchItem.fromDocument);
        }

        return embedded
          ? [...embedded, this.fromDocument(doc)]
          : [this.fromDocument(doc)];
      })
      .flat();
  }

  static fromDocument(doc: AnyDocument): EntitySearchItem {
    if (
      "PDFoundry" in ui &&
      "data" in doc &&
      "pdfoundry" in (doc.data as any).flags
    ) {
      return new PDFoundySearchItem({
        id: doc.id as string,
        uuid: doc.uuid,
        name: doc.name as string,
        documentType: doc.documentName as DocumentType,
        //@ts-expect-error data is merged wih doc
        img: doc.img,
        folder: doc.folder || undefined,
      });
    }
    return new EntitySearchItem({
      id: doc.id as string,
      uuid: doc.uuid,
      name: doc.name as string,
      documentType: doc.documentName as DocumentType,
      //@ts-expect-error data is merged wih doc
      subType: doc.type,
      //@ts-expect-error data is merged wih doc
      img: doc.img,
      folder: doc.folder || undefined,
      system:
        "system" in doc ? (doc.system as Record<string, unknown>) : undefined,
    });
  }
  // Get the drag data for drag operations
  get dragData(): Record<string, string> {
    return {
      type: this.documentType,
      uuid: this.uuid,
    };
  }

  get icon(): string {
    return `<i class="fas ${
      documentIcons[this.documentType]
    } entity-icon"></i>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@${this.documentType}[${this.id}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `game.${DocumentMeta[this.documentType].collection}.get("${
      this.id
    }")`;
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    if (this.folder) {
      return `${this.folder.name}`;
    }
    return getDirectoryName(this.documentType);
  }

  async show(): Promise<void> {
    showDocument(await this.get());
  }

  async get(): Promise<AnyDocument | undefined> {
    return getCollectionFromType(this.documentType).get(this.id) as AnyDocument;
  }

  constructor(data: SearchItemData & { folder?: Folder }) {
    super(data);

    const folder = data.folder;
    if (folder && folder.id) {
      this.folder = {
        id: folder.id,
        name: folder.name,
      };
    }
  }
}

export class PDFoundySearchItem extends EntitySearchItem {
  get icon(): string {
    return `<img class="pdf-thumbnail" src="modules/pdfoundry/assets/pdf_icon.svg" alt="PDF Icon">`;
  }
  get journalLink(): string {
    return `@PDF[${this.name}|page=1]{${this.name}}`;
  }
  async show(): Promise<void> {
    const entity = await this.get();
    (ui as any)?.PDFoundry.openPDFByName(this.name, { entity });
  }
}

export class CompendiumSearchItem extends SearchItem {
  package: string;
  packageName: string;

  static fromCompendium(
    pack: CompendiumPack
  ): CompendiumSearchItem[] | EmbeddedCompendiumSearchItem[] {
    const cIndex = pack.index;

    return cIndex
      .map((item) => {
        const embedded = extractEmbeddedIndex(item, pack);
        const searchItem = new CompendiumSearchItem(pack, item);
        return embedded ? [searchItem, embedded] : searchItem;
      })
      .flat(2);
  }

  constructor(
    pack: CompendiumPack,
    item: {
      _id: string;
      name?: string;
      img?: string | null;
      type?: string;
      system?: Record<string, unknown>;
    }
  ) {
    const packName = pack.collection;
    super({
      id: item._id,
      uuid: `Compendium.${packName}.${item._id}`,
      name: item.name as string,
      documentType: pack.metadata.type as DocumentType,
      subType: item.type,
      img: item.img,
      system: item.system,
    });
    this.package = packName;
    this.packageName = pack?.metadata?.label || pack.title;
    this.documentType = pack.metadata.type as DocumentType;
    this.uuid = `Compendium.${this.package}.${this.id}`;
  }

  // Get the drag data for drag operations
  get dragData(): Record<string, string> {
    return {
      type: this.documentType,
      uuid: this.uuid,
    };
  }

  get icon(): string {
    return `<i class="fas ${
      documentIcons[this.documentType]
    } entity-icon"></i>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@UUID[${this.uuid}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `fromUuid("${this.uuid}")`; // TODO: note that this is async somehow?
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    return `${this.packageName}`;
  }

  async show(): Promise<void> {
    showDocument(await this.get());
  }

  async get(): Promise<AnyDocument | null> {
    return (await fromUuid(this.uuid)) as AnyDocument;
  }
}

export class EmbeddedEntitySearchItem extends SearchItem {
  folder?: { id: string; name: string };

  #tagline?: string;
  #embeddedName: string;

  static fromDocument(document: AnyDocument) {
    if (!document.parent || !document.id) {
      throw new Error("Not properly embedded");
    }

    const number = [...document.parent[document.collectionName].keys()].indexOf(
      document.id
    );

    const parentType = document.parent.documentName as DocumentType;

    return new EmbeddedEntitySearchItem({
      id: document.id,
      uuid: document.uuid,
      parentName: document.parent.name || undefined,
      embeddedName: document.name,
      type: parentType,
      tagline: `Pg. ${number} - ${
        document.parent.folder?.name || getDirectoryName(parentType)
      }`,
      folder: document.parent.folder || undefined,
    });
  }

  constructor(item: {
    id: string;
    uuid: string;
    img?: string | null;
    type?: string;
    parentName?: string;
    embeddedName: string;
    tagline?: string;
    folder?: Folder;
  }) {
    super({
      id: item.id,
      uuid: item.uuid,
      name: `${item.embeddedName} | ${item.parentName}`,
      documentType: item.type as DocumentType,
      img: item.img,
    });

    const folder = item.folder;
    if (folder && folder.id) {
      this.folder = {
        id: folder.id,
        name: folder.name,
      };
    }

    this.#embeddedName = item.embeddedName;
    this.#tagline = item.tagline;
  }

  // Get the drag data for drag operations
  get dragData(): Record<string, string> {
    return {
      // TODO: Use type from index
      type: "JournalEntryPage",
      uuid: this.uuid,
    };
  }

  get icon(): string {
    // TODO: Add table tor subtypes
    return `<i class="fa-duotone fa-book-open entity-icon"></i>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@UUID[${this.uuid}]{${this.#embeddedName}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `fromUuid("${this.uuid}")`;
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    return this.#tagline || "";
  }

  get tooltip(): string {
    const type = this.subType
      ? getSubTypeName(this.documentType, this.subType)
      : loc(DocumentMeta[this.documentType].label);

    const page = loc(CONFIG.JournalEntryPage.documentClass.metadata.label);
    return `${type} ${page} - ${this.#tagline}`;
  }

  async show(): Promise<void> {
    showDocument(await this.get());
  }

  async get(): Promise<AnyDocument | null> {
    return (await fromUuid(this.uuid)) as AnyDocument;
  }
}

export class EmbeddedCompendiumSearchItem extends SearchItem {
  package: string;
  packageName: string;

  // Inject overrides??
  #tagline?: string;

  static fromDocument(document: AnyDocument): EmbeddedCompendiumSearchItem {
    if (!document.parent) {
      throw new Error("Document is not embedded");
    }
    if (!document.pack) {
      throw new Error("Document has no pack");
    }
    const pack = game.packs.get(document.pack);
    if (!pack) {
      throw new Error("Document has invalid pack");
    }

    const number = [...document.parent[document.collectionName].keys()].indexOf(
      document.id
    );

    return new EmbeddedCompendiumSearchItem(pack, {
      _id: document.id as string,
      parentName: document.parent.name || undefined,
      embeddedName: document.name,
      parentId: document.parent.id as string,
      type: "JournalEntryPage",
      tagline: `Pg. ${number} - ${pack?.metadata?.label || pack.title}`,
    });
  }

  constructor(
    pack: CompendiumPack,
    item: {
      _id: string;
      img?: string | null;
      type?: string;
      parentId: string;
      parentName?: string;
      embeddedName: string;
      tagline?: string;
    }
  ) {
    const packName = pack.collection;
    const uuid = `Compendium.${packName}.${item.parentId}.${item.type}.${item._id}`;
    super({
      id: item._id,
      uuid,
      name: `${item.embeddedName} | ${item.parentName}`,
      documentType: item.type as DocumentType,
      img: item.img,
    });
    this.uuid = uuid;
    this.package = packName;
    this.packageName = pack?.metadata?.label || pack.title;
    this.documentType = pack.metadata.type as DocumentType;
    this.#tagline = item.tagline;
  }

  // Get the drag data for drag operations
  get dragData(): Record<string, string> {
    return {
      // TODO: Use type from index
      type: "JournalEntryPage",
      uuid: this.uuid,
    };
  }

  get icon(): string {
    // TODO: Add table tor subtypes
    return `<i class="fa-duotone fa-book-open entity-icon"></i>`;
  }

  // Reference the entity in a journal, chat or other places that support it
  get journalLink(): string {
    return `@UUID[${this.uuid}]{${this.name}}`;
  }

  // Reference the entity in a script
  get script(): string {
    return `fromUuid("${this.uuid}")`; // TODO: note that this is async somehow?
  }

  // Short tagline that explains where/what this is
  get tagline(): string {
    return this.#tagline || `${this.packageName}`;
  }

  get tooltip(): string {
    const type = this.subType
      ? getSubTypeName(this.documentType, this.subType)
      : loc(DocumentMeta[this.documentType].label);

    const page = loc(CONFIG.JournalEntryPage.documentClass.metadata.label);
    return `${type} ${page} - ${this.#tagline}`;
  }

  async show(): Promise<void> {
    showDocument(await this.get());
  }

  async get(): Promise<AnyDocument | null> {
    return (await fromUuid(this.uuid)) as AnyDocument;
  }
}

export function searchItemFromDocument(document: AnyDocument): SearchItem {
  if (document.parent) {
    if (document.compendium) {
      return EmbeddedCompendiumSearchItem.fromDocument(document);
    }
    return EmbeddedEntitySearchItem.fromDocument(document);
  }
  if (document.compendium) {
    return new CompendiumSearchItem(document.compendium, {
      _id: document.id as string,
      name: document.name,
      //@ts-expect-error it exists on most docs, it's ok if it's undefined
      img: document.img,
      //@ts-expect-error it exists on most docs, it's ok if it's undefined
      type: document.type,
    });
  }
  return EntitySearchItem.fromDocument(document);
}

export function isEntity(item: SearchItem): item is EntitySearchItem {
  return item instanceof EntitySearchItem;
}
export function isEmbeddedEntity(
  item: SearchItem
): item is EmbeddedEntitySearchItem {
  return item instanceof EmbeddedEntitySearchItem;
}
export function isCompendiumEntity(
  item: SearchItem
): item is CompendiumSearchItem {
  return item instanceof CompendiumSearchItem;
}

export function isEmbeddedCompendiumEntity(
  item: SearchItem
): item is EmbeddedCompendiumSearchItem {
  return item instanceof EmbeddedCompendiumSearchItem;
}

export interface SearchResult {
  item: SearchItem;
  formattedMatch?: string;
}

class FuseSearchIndex {
  everything: SearchResult[] = [];
  fuse: Fuse<SearchItem> = new Fuse([], {
    keys: ["name"],
    includeMatches: true,
    threshold: 0.3,
  });

  #formatMatch(matches: readonly Fuse.FuseResultMatch[]) {
    const match = matches[0];
    if (!match?.value) return;

    let text = match.value;
    [...match.indices].reverse().forEach(([start, end]) => {
      text =
        text.substring(0, start) +
        `<strong>${text.substring(start, end + 1)}</strong>` +
        text.substring(end + 1);
    });
    return text;
  }

  getSize() {
    return this.everything.length;
  }

  addAll(items: SearchItem[]) {
    for (const item of items) {
      this.add(item);
    }
  }

  add(item: SearchItem): void {
    this.fuse.add(item);
    this.everything.push({ item });
  }

  replaceItem(item: SearchItem): void {
    // Remove/add
    this.fuse.remove((i) => i?.uuid == item.uuid);
    this.fuse.add(item);
    // Replace
    const index = this.everything.findIndex(
      (result) => result.item.uuid === item.uuid
    );
    this.everything[index] = { item };
  }

  removeByUuid(uuid: string): void {
    this.fuse.remove((i) => i?.uuid == uuid);
    const index = this.everything.findIndex(
      (result) => result.item.uuid === uuid
    );

    if (index > -1) {
      this.everything.splice(index, 1);
    }
  }

  search(query: string): SearchResult[] {
    if (query === "") {
      return this.everything;
    }
    return this.fuse.search(query).map((res) => ({
      item: res.item,
      formattedMatch: this.#formatMatch(res.matches || []),
    }));
  }
}

class FuzzySortSearchIndex {
  everything: SearchItem[] = [];

  getSize() {
    return this.everything.length;
  }

  addAll(items: SearchItem[]) {
    for (const item of items) {
      this.add(item);
    }
  }

  add(item: SearchItem): void {
    this.everything.push(item);
  }

  replaceItem(item: SearchItem): void {
    // Replace
    const index = this.everything.findIndex(
      (result) => result.uuid === item.uuid
    );
    this.everything[index] = item;
  }

  removeByUuid(uuid: string): void {
    const index = this.everything.findIndex((result) => result.uuid === uuid);

    if (index > -1) {
      this.everything.splice(index, 1);
    }
  }

  search(query: string): SearchResult[] {
    if (query === "") {
      return this.everything.map((item) => ({ item }));
    }

    return fuzzysort
      .go(query, this.everything, {
        key: "name",
        all: true,
        threshold: 0.5,
      })
      .map((res) => {
        return {
          item: res.obj,
          formattedMatch: res.highlight("<strong>", "</strong>"),
        };
      });
  }
}

export class SearchLib {
  index: FuseSearchIndex | FuzzySortSearchIndex;
  systemFields = systemFields.reduce((result, registreredField) => {
    result[registreredField.documentType] =
      result[registreredField.documentType] || [];
    result[registreredField.documentType].push(
      `system.${registreredField.indexName}`
    );
    return result;
  }, {} as Record<string, string[]>);

  constructor() {
    const engine = getSetting(ModuleSetting.SEARCH_ENGINE);
    if (engine === "fuzzysort") {
      this.index = new FuzzySortSearchIndex();
    } else {
      this.index = new FuseSearchIndex();
    }
  }

  indexCompendium(compendium?: CompendiumPack): void {
    if (!compendium) return;
    if (packEnabled(compendium)) {
      const index = CompendiumSearchItem.fromCompendium(compendium);
      this.index.addAll(index);
    }
  }

  async indexCompendiums(): Promise<void> {
    if (!game.packs) return;

    for await (const res of loadIndexes(this.systemFields)) {
      if (res.error) {
        console.log("Quick Insert | Index loading failure", res);
        continue;
      }
      this.indexCompendium(game.packs.get(res.pack));
    }
  }

  indexDocuments(): void {
    if (!directoryEnabled()) return;

    for (const type of enabledDocumentTypes()) {
      this.index.addAll(
        EntitySearchItem.fromDocuments(
          getCollectionFromType(type).contents as AnyDocument[]
        )
      );
    }
  }

  addItem(item: SearchItem): void {
    this.index.add(item);
  }

  removeItem(entityUuid: string): void {
    this.index.removeByUuid(entityUuid);
  }

  replaceItem(item: SearchItem): void {
    this.index.replaceItem(item);
  }

  search(
    text: string,
    filter: SearchResultPredicate | null,
    max: number
  ): SearchResult[] {
    if (filter) {
      return this.index.search(text).filter(filter).slice(0, max);
    }
    return this.index.search(text).slice(0, max);
  }
}

interface IndexStatus {
  error?: unknown;
  pack: string;
  packsLeft: number;
  errorCount: number;
}

export async function* loadIndexes(
  systemFields?: Record<string, string[]>
): AsyncGenerator<IndexStatus> {
  if (!game.packs) {
    console.error("Can't load indexes before packs are initialized");
    return;
  }

  // Information about failures
  const failures: {
    [pack: string]: {
      errors: number;
      waiting?: Promise<unknown>;
    };
  } = {};

  const timeout = 1000;

  const packsRemaining: CompendiumPack[] = [];
  for (const pack of game.packs) {
    if (packEnabled(pack)) {
      failures[pack.collection] = { errors: 0 };
      packsRemaining.push(pack);
    }
  }

  while (packsRemaining.length > 0) {
    const pack = packsRemaining.shift();
    if (!pack) break;

    let promise: Promise<unknown> | undefined;

    try {
      let options: any;
      if (getSetting(ModuleSetting.EMBEDDED_INDEXING)) {
        if (pack.documentClass.documentName === "JournalEntry") {
          options = { fields: ["pages.name", "pages._id"] };
        }
      }

      if (systemFields?.[pack.documentClass.documentName]) {
        options = { fields: systemFields[pack.documentClass.documentName] };
      }

      promise = failures[pack.collection].waiting ?? pack.getIndex(options);

      await withDeadline(
        promise,
        timeout * (failures[pack.collection].errors + 1)
      );
    } catch (error) {
      ++failures[pack.collection].errors;
      if (error instanceof TimeoutError) {
        failures[pack.collection].waiting = promise;
      } else {
        delete failures[pack.collection].waiting;
      }

      yield {
        error: error,
        pack: pack.collection,
        packsLeft: packsRemaining.length,
        errorCount: failures[pack.collection].errors,
      };
      if (failures[pack.collection].errors <= 4) {
        // Pack failed, will be retried later.
        packsRemaining.push(pack);
      } else {
        console.warn(
          `Quick Insert | Package "${pack.collection}" could not be indexed `
        );
      }
      continue;
    }

    yield {
      pack: pack.collection,
      packsLeft: packsRemaining.length,
      errorCount: failures[pack.collection].errors,
    };
  }
}
