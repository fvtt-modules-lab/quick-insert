import {
  isEntity,
  isCompendiumEntity,
  isEmbeddedCompendiumEntity,
  type SearchResult,
  isEmbeddedEntity,
} from "./searchLib";
import { FILTER_COMPENDIUM_ALL, FILTER_FOLDER_ROOT } from "./store/Filters";
import type { SearchFilterConfig, SearchFilter } from "./store/Filters";
import { visibleFilters } from "./store/FilterStore";
import { getSystemFields } from "./systemFields";

export class SearchFilterCollection {
  filters: SearchFilter[] = [];

  init() {
    visibleFilters.subscribe((v) => (this.filters = v));
  }

  search(query?: string): SearchFilter[] {
    if (!query) {
      return [...this.filters];
    }
    return this.filters.filter((f) => f.tag.includes(query));
  }

  getFilter(id: string): SearchFilter | undefined {
    return this.filters.find((f) => f.id == id);
  }

  getFilterByTag(tag: string): SearchFilter | undefined {
    return this.filters.filter((f) => !f.disabled).find((f) => f.tag == tag);
  }
}

// Is parentFolder inside targetFolder?
function isInFolder(
  parentFolder: string | undefined | null,
  targetFolder: string
): boolean {
  if (targetFolder === FILTER_FOLDER_ROOT) return true;

  while (parentFolder) {
    if (parentFolder === targetFolder) return true;
    parentFolder = game.folders?.get(parentFolder)?.folder?.id;
  }
  return false;
}

export function matchFilterConfig(
  config: SearchFilterConfig,
  resultItem: SearchResult
): boolean {
  let entityMatch = true;

  if (config.documentTypes.length) {
    entityMatch =
      config.documentTypes.includes(resultItem.item.documentType) ||
      Boolean(
        resultItem.item.subType &&
          config.documentTypes.includes(
            `${resultItem.item.documentType}:${resultItem.item.subType}`
          )
      );
  }

  if (!entityMatch) {
    return false;
  }

  const filterByLocation = Boolean(
    config.folders.length + config.compendiums.length
  );

  if (filterByLocation) {
    let locationMatch = false;
    if (isEntity(resultItem.item) || isEmbeddedEntity(resultItem.item)) {
      const filterRoot =
        config.folders.length === 1 && config.folders[0] === FILTER_FOLDER_ROOT;
      if (filterRoot) {
        locationMatch = true;
      } else {
        for (const f of config.folders) {
          if (isInFolder(resultItem.item.folder?.id, f)) {
            locationMatch = true;
            break;
          }
        }
      }
    } else if (
      isCompendiumEntity(resultItem.item) ||
      isEmbeddedCompendiumEntity(resultItem.item)
    ) {
      const filterAll =
        config.compendiums.length === 1 &&
        config.compendiums[0] === FILTER_COMPENDIUM_ALL;

      locationMatch =
        filterAll || config.compendiums.includes(resultItem.item.package);
    }
    if (!locationMatch) {
      return false;
    }
  }

  const fieldsToMatch = getSystemFields(resultItem.item.documentType);

  const filterBySystemExtension =
    fieldsToMatch.length !== 0 &&
    config.system &&
    Object.keys(config.system).length;

  if (filterBySystemExtension && resultItem.item.system) {
    return fieldsToMatch.every((f) => {
      const field = f.indexName;
      return (
        config.system?.[field] !== undefined &&
        resultItem.item.system?.[field] !== undefined &&
        resultItem.item.system?.[field] === config.system[field]
      );
    });
  }

  return true;
}
