import { type DocumentAction, defaultAction, getActions } from "./actions";
import type { SearchResult } from "./searchLib";
import { EntitySearchItem, DocumentType } from "./searchLib";
import { randomId } from "./utils";

export type CombinedSearchResult = SearchResult & {
  actions: DocumentAction[];
  defaultAction: string;
};

type BaseData = {
  name: string;
  documentType: DocumentType;
  img?: string;
};

const basicData = {
  item: {
    name: "Shield",
    documentType: DocumentType.ITEM,
    img: "icons/equipment/shield/heater-steel-boss-red.webp",
  },
  actor: {
    name: "Forest Giant",
    documentType: DocumentType.ACTOR,
    img: "icons/creatures/magical/humanoid-giant-forest-blue.webp",
  },
  journalEntry: {
    name: "JournalEntry 1",
    documentType: DocumentType.JOURNALENTRY,
  },
  macro: {
    name: "Macro 1",
    documentType: DocumentType.MACRO,
  },
  rollTable: {
    name: "RollTable 1",
    documentType: DocumentType.ROLLTABLE,
  },
  scene: {
    name: "Scene 1",
    documentType: DocumentType.SCENE,
  },
};

function makeNew(base: BaseData, insertContext = false): CombinedSearchResult {
  const id = randomId(10);
  const item = new EntitySearchItem({
    id: id,
    uuid: id,
    img: null,
    ...base,
  });

  item.__source = "fake";

  return {
    item,
    actions: getActions(base.documentType, insertContext),
    defaultAction: defaultAction(base.documentType, insertContext),
  };
}

export function fakeSearchResults(
  insertContext = false
): CombinedSearchResult[] {
  return [
    makeNew(basicData.item, insertContext),
    makeNew(basicData.actor, insertContext),
    makeNew(basicData.journalEntry, insertContext),
    makeNew(basicData.macro, insertContext),
    makeNew(basicData.rollTable, insertContext),
    makeNew(basicData.scene, insertContext),
  ];
}
