import { DocumentType } from "./searchLib";
import { loc } from "./utils";

export function typeLabel(type: string, subType?: string) {
  if (type.includes(":")) {
    [type, subType] = type.split(":");
  } else if (!subType) {
    return loc(`DOCUMENT.${type}`);
  }

  //@ts-expect-error can't be arsed to type
  return loc(CONFIG[type]?.typeLabels?.[subType] || subType);
}

export function getSubTypes(type: DocumentType): string[] {
  if (
    type === DocumentType.JOURNALENTRY ||
    type === DocumentType.MACRO ||
    type === DocumentType.ROLLTABLE ||
    type === DocumentType.PLAYLIST ||
    type === DocumentType.SCENE ||
    type === DocumentType.ADVENTURE
  ) {
    return [];
  }

  if (Array.isArray(game.system.documentTypes[type])) {
    return (
      game.system.documentTypes[type]
        .filter((t) => t !== "base")
        .map((subType) => `${type}:${subType}`) || []
    );
  }
  return Object.keys(game.system.documentTypes[type] as object).map(
    (subType) => `${type}:${subType}`
  );
}
