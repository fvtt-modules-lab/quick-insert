import { mloc } from "module/utils";

const AppV2 = foundry.applications.api.ApplicationV2;

export class AboutApp extends AppV2 {
  static DEFAULT_OPTIONS = {
    id: "qi-about-app",
    classes: ["application"],
    window: {
      title: "QUICKINSERT.AboutTitle",
      frame: true,
      positioned: true,
      minimizable: false,
      resizable: false,
      content: false,
    },
    position: {
      width: 350,
    },
  };

  override async _renderHTML(): Promise<string> {
    return "";
  }

  override _replaceHTML(result: string, content: HTMLElement) {
    content.innerHTML = `
    <img src="../modules/quick-insert/images/fvtt-modules-lab.png">
        <p>${mloc("AboutThanks")}</p>
        <p>${mloc("AboutDescription")}</p>
        <ul>
            <li>
                <a href="https://discord.gg/jM4XQ33EjK" target="_blank">
                  ${mloc("AboutDiscord")}
                </a>
            </li>
            <li>
                <a href="https://gitlab.com/fvtt-modules-lab/quick-insert/-/issues" target="_blank">
                  ${mloc("AboutIssues")}
                </a>
            </li>
            <li>
                <a href="https://ko-fi.com/sunspots" target="_blank">
                  ${mloc("AboutSupport")}
                </a>
            </li>
        <ul>
    `;
  }
}

export function openAboutApp() {
  new AboutApp({}).render(true);
}
