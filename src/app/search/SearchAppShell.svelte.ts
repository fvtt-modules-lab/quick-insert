import { mount, unmount } from "svelte";
import { get, type Unsubscriber } from "svelte/store";
import { ContextMode, identifyContext, SearchContext } from "module/contexts";

import { getSetting } from "module/settings";
import { stores } from "module/store/SettingsStore";
import { ModuleSetting } from "module/store/ModuleSettings";
import type { SearchFilter } from "module/store/Filters";
import {
  FilterPopupApp,
  openFilterPopup,
} from "app/filterpopup/FilterPopupApp.svelte";
import { makeEmptyFilter } from "app/settings/filters/filterUtils";

import SearchApp from "./SearchApp.svelte";

export class SearchAppShell {
  #unmount?: Record<string, any>;

  #filterEditor?: FilterPopupApp;

  #densitySub?: Unsubscriber;

  // Rendering
  element?: HTMLElement;
  rendered = false;
  visible = false;

  debug = false;
  rememberedText = "";
  rememberedFilter?: SearchFilter = undefined;

  attachedContext: SearchContext | null = null;

  props: {
    context?: SearchContext | undefined;
    filter?: SearchFilter | undefined;
    tooltips?: "RIGHT" | "LEFT";
    filterEditorOpen?: boolean;
  } = $state({
    context: undefined,
    filter: undefined,
  });

  // Moving window state
  #moveInitial = { x: 0, y: 0, width: 0 };
  #movePosisition = { left: 0, bottom: 0 };

  // On mouse down, retain focus.
  #retainFocus() {
    const element = document.activeElement;
    if (element?.isConnected && element?.closest(".search-editable-input")) {
      setTimeout(() => element && (element as HTMLInputElement).focus());
    }
  }

  // Dragging search results
  #dragStart = (event: DragEvent) => {
    if ((event.target as HTMLElement)?.closest(".quick-insert-result")) {
      this.element?.classList.add("dragging");
    }
  };

  #dragEnd = () => {
    this.element?.classList.remove("dragging");
  };

  #drop = (event: DragEvent) => {
    if ((event.target as HTMLElement)?.closest(".quick-insert-app")) {
      return;
    }
    if (this.element?.classList.contains("dragging")) {
      this.element.classList.remove("dragging");
      if (!event.shiftKey) {
        this.close();
      }
    }
  };

  // Drag-move window drop/end
  #moveEnd = (event?: MouseEvent) => {
    event?.stopPropagation();
    event?.stopImmediatePropagation();

    window.removeEventListener("pointermove", this.#move);
    window.removeEventListener("pointerup", this.#moveEnd);
  };

  // Drag-move window moving
  #move = (event: MouseEvent) => {
    if (!this.element) {
      this.#moveEnd();
      return;
    }

    const diff = {
      x: event.clientX - this.#moveInitial.x,
      y: event.clientY - this.#moveInitial.y,
    };

    const newPos = {
      left: this.#movePosisition.left + diff.x,
      bottom: this.#movePosisition.bottom - diff.y,
    };

    this.setPosition(newPos);
  };

  // Drag-move window start
  #moveStart = (event: MouseEvent) => {
    this.#retainFocus();

    if (!this.element) {
      return;
    }
    const rect = this.element.getBoundingClientRect();
    this.#moveInitial = {
      x: event.clientX,
      y: event.clientY,
      width: rect.width,
    };
    this.#movePosisition = {
      left: rect.left,
      bottom: window.innerHeight - rect.bottom,
    };

    window.addEventListener("pointermove", this.#move);
    window.addEventListener("pointerup", this.#moveEnd);
  };

  // TODO: Add persistence
  setPosition({ left, bottom }: { left: number; bottom: number }) {
    if (!this.element) {
      return;
    }

    left = Math.min(
      Math.max(left, 0),
      window.innerWidth - this.#moveInitial.width
    );
    bottom = Math.min(Math.max(bottom, 0), window.innerHeight - 150);

    this.element.style.left = `${left}px`;
    this.element.style.bottom = `${bottom}px`;
    this.element.style.maxHeight = `min(${
      window.innerHeight - bottom
    }px, calc(50vh + var(--input-height)))`;
  }

  async render(options?: { context?: SearchContext }) {
    if (this.rendered) {
      return;
    }

    if (options && options.context) {
      this.attachedContext = options.context;
    } else {
      // Try to infer context
      const target = document.activeElement;
      if (target) {
        this.attachedContext = identifyContext(target);
      }
    }

    this.renderContents();
  }

  close() {
    if (this.#filterEditor) {
      this.#filterEditor.close();
      this.#filterEditor = undefined;
    }
    this.attachedContext?.onClose?.();
    game.tooltip.deactivate();

    // Clear drag listeners
    document.removeEventListener("dragstart", this.#dragStart);
    document.removeEventListener("dragend", this.#dragEnd);
    document.removeEventListener("drop", this.#drop);

    if (this.#unmount) {
      unmount(this.#unmount, { outro: true }).then(() => {
        this.rendered = false;
        this.visible = false;
        if (this.element) {
          this.element.style.display = "none";
        }
      });
    }
  }

  async openFilterEditor(filter: SearchFilter) {
    filter = filter || makeEmptyFilter();
    if (!this.#filterEditor) {
      this.props.filterEditorOpen = true;

      this.#filterEditor = await openFilterPopup(
        filter,
        (changedFilter) => (this.props.filter = changedFilter),
        () => {
          this.#filterEditor = undefined;
          this.props.filterEditorOpen = false;
        },
        this.element!.getBoundingClientRect()
      );
    }
  }

  closeFilterEditor() {
    this.#filterEditor?.close();
    this.#filterEditor = undefined;
    this.props.filterEditorOpen = false;
  }

  focus() {
    this.element?.querySelector("input")?.focus();
  }

  clickOutside() {
    if (this.visible && !this.#filterEditor && !this.debug) {
      this.close();
    }
  }

  renderContents() {
    if (!this.element) {
      this.element = document.createElement("div");
      this.element.id = "qi-search-appv3";
      this.element.className = "quick-insert-app application";
      document.body.appendChild(this.element);
    }
    if (
      getSetting(ModuleSetting.REMEMBER_BROWSE_INPUT) &&
      this.attachedContext &&
      this.attachedContext.mode === ContextMode.Browse
    ) {
      this.attachedContext.startText =
        this.attachedContext.startText || this.rememberedText;
      this.attachedContext.filter =
        this.attachedContext.filter || this.rememberedFilter;
    }

    this.props.context = this.attachedContext || undefined;
    this.props.filter = this.attachedContext?.filter;

    this.#unmount = mount(SearchApp, {
      target: this.element,
      props: this.props,
      intro: true,
      events: {
        close: () => this.close(),
        focusLost: () => {},
        openFilterEditor: async (event: CustomEvent) => {
          const filter = event.detail;
          if (this.#filterEditor) {
            this.closeFilterEditor();
          } else {
            this.openFilterEditor(filter);
          }
        },
        searched: (event: CustomEvent) => {
          if (this.attachedContext?.mode === ContextMode.Browse) {
            this.rememberedText = event.detail || "";
          }
        },
        setFilter: (event: CustomEvent) => {
          if (this.attachedContext?.mode === ContextMode.Browse) {
            this.rememberedFilter = event.detail || undefined;
          }
        },
        moveStart: (event: CustomEvent) => {
          this.#moveStart(event.detail);
        },
      },
    });

    setTimeout(() => {
      this.focus();
      if (this.attachedContext?.mode === ContextMode.Browse) {
        this.element?.querySelector("input")?.select();
      }
    });

    if (!this.element) return;

    // (Re-)set position
    this.element.removeAttribute("style");

    // Load context
    this.applyContext();

    // Setup drag listeners
    document.addEventListener("dragstart", this.#dragStart);
    document.addEventListener("dragend", this.#dragEnd);
    document.addEventListener("drop", this.#drop);

    if (!this.#densitySub) {
      this.#densitySub = stores[ModuleSetting.SEARCH_DENSITY].subscribe(() =>
        this.updateDensity()
      );
    } else {
      this.updateDensity();
    }
    this.rendered = true;
    setTimeout(() => (this.visible = true));
  }

  applyContext() {
    if (!this.element) return;

    if (this.attachedContext?.spawnCSS) {
      //@ts-expect-error FIXME
      globalThis.$(this.element).css(this.attachedContext.spawnCSS);
    }

    this.attachedContext?.classes?.forEach((className) =>
      this.element?.classList.add(className)
    );

    let left = this.attachedContext?.spawnCSS?.left;
    if (typeof left === "string") {
      left = parseInt(left, 10);
    }
    const tooltips = left !== undefined && left < 350 ? "RIGHT" : "LEFT";
    this.props.tooltips = tooltips;
  }

  updateDensity() {
    const density = get(stores[ModuleSetting.SEARCH_DENSITY]);
    if (density) {
      this.element?.classList.remove("density-compact");
      this.element?.classList.remove("density-comfortable");
      this.element?.classList.remove("density-spacious");
      this.element?.classList.add("density-" + density);
    }
  }
}
