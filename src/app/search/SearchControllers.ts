import { loc } from "module/utils";
import { ContextMode, SearchContext } from "module/contexts";
import { QuickInsert } from "module/core";

import {
  DOCUMENTACTIONS,
  getActions,
  getDefaultActions,
  type DocumentAction,
} from "module/actions";
import type { SearchItem, SearchResult } from "module/searchLib";
import type { SearchFilter } from "module/store/Filters";
import { matchFilterConfig } from "module/searchFilters";
import { calculate } from "module/mathEval";
import { searchPlaylists } from "module/audioIndex";

import SearchResults from "./SearchResults.svelte";
import SearchFiltersResults from "./SearchFiltersResults.svelte";
import SearchHelpResults from "./SearchHelpResults.svelte";
import SearchCommandResults from "./SearchCommandResults.svelte";
import SearchCalcResults from "./SearchCalcResults.svelte";
import SearchSlashResults from "./SearchSlashResults.svelte";
import type { Component } from "svelte";
import SearchAudioResults from "./SearchAudioResults.svelte";

export enum SearchMode {
  DOCUMENT = "DOCUMENT",
  FILTER = "FILTER",
  HELP = "HELP",
  COMMANDS = "COMMANDS",
  SLASH = "SLASH",
  CALC = "CALC",
  AUDIO = "AUDIO",
}

export interface SearchControllerOptions {
  getContext: () => SearchContext | undefined;
  close: () => void;
  setFilter: (filter: SearchFilter) => void;
  setInputText: (inputText: string) => void;
  refresh: () => void;
}

export interface SearchResultItem {
  item: any;
  formattedMatch?: string;
  actions?: DocumentAction[];
  defaultAction?: string;
}

export abstract class SearchController {
  abstract onTab(item: SearchResultItem): void;
  abstract onAction(item: unknown, action?: string, shiftKey?: boolean): void;
  abstract search(textInput: string, ...args: unknown[]): SearchResultItem[];
}

export class DocumentController extends SearchController {
  #close: () => void;
  #context?: SearchContext;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onTab(_item: SearchResultItem): void {}

  public get isInsertMode(): boolean {
    return this.#context?.mode == ContextMode.Insert;
  }

  constructor(options: SearchControllerOptions) {
    super();
    this.#context = options.getContext();
    this.#close = options.close;
  }

  async onAction(
    item: SearchItem,
    action: string | undefined,
    shiftKey: boolean
  ): Promise<void> {
    if (!action) {
      return;
    }
    const searchItem = item;

    console.info(
      `Quick Insert | Invoked Action [${action}] on [${searchItem.name}] shiftKey:${shiftKey}`
    );
    const val = await DOCUMENTACTIONS[action](searchItem);

    if (val && this.isInsertMode) {
      // this.app.keepOpen = shiftKey; // Keep open until onSubmit completes
      this.#context?.onSubmit(val);
    }

    if (!shiftKey || this.#context?.allowMultiple === false) {
      this.#close();
    }
  }

  search = (textInput: string, filter?: SearchFilter): SearchResultItem[] => {
    if (!QuickInsert.searchLib) return [];
    textInput = textInput.trim();
    if (!filter && textInput.length == 0) {
      return [];
    }
    // Set a lower maximum if search is zero or one char (single-character search is fast, but rendering is slow).
    const max = textInput.length <= 1 ? 20 : 100;
    let results: SearchResult[] = [];
    if (filter) {
      if (filter.filterConfig) {
        results = QuickInsert.searchLib.search(
          textInput,
          (item) =>
            filter?.filterConfig
              ? matchFilterConfig(filter.filterConfig, item)
              : true,
          max
        );
      }
    } else {
      results = QuickInsert.searchLib.search(textInput, null, max);
    }

    if (
      this.#context?.restrictTypes &&
      this.#context?.restrictTypes.length > 0
    ) {
      results = results.filter((i) =>
        this.#context?.restrictTypes?.includes(i.item.documentType)
      );
    }

    const defaultActions = getDefaultActions();
    return results
      .map((res) => {
        const actions = getActions(res.item.documentType, this.isInsertMode);
        return {
          item: res.item,
          formattedMatch: res.formattedMatch,
          actions: actions,
          defaultAction:
            (!this.isInsertMode && defaultActions[res.item.documentType]) ||
            actions[actions.length - 1].id,
        };
      })
      .reverse();
  };
}

export class FilterController extends SearchController {
  setFilter: (filter: SearchFilter) => void;

  constructor(callbacks: SearchControllerOptions) {
    super();
    this.setFilter = callbacks.setFilter;
  }

  onTab(item: SearchResultItem): void {
    this.setFilter(item.item);
  }

  onAction(item: SearchFilter): void {
    this.setFilter(item);
  }

  search(textInput: string): SearchResultItem[] {
    const cleanedInput = textInput.replace("@", "").toLowerCase().trim();
    if (/\s$/g.test(textInput)) {
      // User has added a space after tag -> selected
      const matchingFilter = QuickInsert.filters.getFilterByTag(cleanedInput);
      if (matchingFilter) {
        this.setFilter(matchingFilter);
        return [];
      }
    }

    return QuickInsert.filters.filters
      .filter((f) => !f.disabled)
      .filter((f) => f.tag.includes(cleanedInput))
      .map((f) => ({ item: f }));
  }
}

interface HelpItem {
  id: string;
  name: string;
  description: string;
  inputString: string;
}
const helpItems: HelpItem[] = [
  {
    id: "/",
    name: "/",
    description: "SearchHelpChatCommand",
    inputString: "/",
  },
  {
    id: ">",
    name: ">",
    description: "SearchHelpKeyboardCommand",
    inputString: ">",
  },
  {
    id: "@",
    name: "@",
    description: "SearchHelpSearchFilter",
    inputString: "@",
  },
  {
    id: "=",
    name: "=",
    description: "SearchHelpCalculator",
    inputString: "=",
  },
  {
    id: "#",
    name: "#",
    description: "SearchHelpAudio",
    inputString: "#",
  },
];

export class HelpController extends SearchController {
  setInputText: (inputText: string) => void;

  constructor(callbacks: SearchControllerOptions) {
    super();
    this.setInputText = callbacks.setInputText;
  }

  onTab(item: SearchResultItem): void {
    this.setInputText(item.item.inputString);
  }

  onAction(item: HelpItem): void {
    this.setInputText(item.inputString);
  }

  search(textInput: string): SearchResultItem[] {
    const cleanedInput = textInput.replace("?", "").toLowerCase().trim();
    return helpItems
      .filter((item) => item.id.startsWith(cleanedInput))
      .map((item) => ({ item }));
  }
}

interface KeybindItem {
  onDown: ClientKeybindings.KeybindingActionConfig["onDown"];
}

const moduleAilases: Record<string, string | unknown> = {
  "quick-insert": "Quick Insert",
};
export class KeybindController extends SearchController {
  static stringKeybinds(binds?: ClientKeybindings.KeybindingActionBinding[]) {
    return (
      binds?.map((k) =>
        //@ts-expect-error Don't care if protected, I need it
        KeybindingsConfig._humanizeBinding(k)
      ) || []
    );
  }

  close: () => void;

  constructor(callbacks: SearchControllerOptions) {
    super();
    this.close = callbacks.close;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onTab(_item: SearchResultItem): void {}

  onAction(
    item: KeybindItem,
    _action: string | undefined,
    shiftKey: boolean
  ): void {
    try {
      item.onDown?.({} as unknown as KeyboardManager.KeyboardEventContext);
    } catch (error) {
      ui.notifications?.error("Failed to execute command");
      console.warn(error);
    }

    if (!shiftKey) this.close();
  }

  search(textInput: string): SearchResultItem[] {
    const cleanedInput = textInput.replace(">", "").toLowerCase().trim();

    const actions = [...game.keybindings.actions]
      .filter(([, conf]) => conf.editable)
      .map(([id, conf]: [string, ClientKeybindings.KeybindingActionConfig]) => {
        if (!conf.namespace) return;

        const namespaceName =
          conf.namespace === "core"
            ? "Core" // TODO: localized string???
            : conf.namespace === game.system.id
            ? game.system.title
            : moduleAilases[conf.namespace] ||
              game.modules.get(conf.namespace)?.title;

        const bindings = game.keybindings.bindings?.get(id);

        return {
          item: {
            id,
            name: `${namespaceName}: ${loc(conf.name)}`,
            namespace: conf.namespace,
            description: conf.hint && loc(conf.hint),
            keybinds: KeybindController.stringKeybinds(bindings),
            onDown: conf.onDown,
          },
        };
      })
      .filter((i): i is SearchResultItem => Boolean(i));

    return actions.filter((item) =>
      item.item.name.toLowerCase().includes(cleanedInput)
    );
  }
}

interface SlashCommand {
  id: string;
  commandText: string;
  description?: string;
  args?: string;
}

const slashCommands: SlashCommand[] = [
  {
    id: "ic",
    commandText: "/ic",
    args: "{message}",
    description: "QUICKINSERT.SearchChatCommandIc",
  },
  {
    id: "ooc",
    commandText: "/ooc",
    args: "{message}",
    description: "QUICKINSERT.SearchChatCommandOoc",
  },
  {
    id: "emote",
    commandText: "/emote",
    args: "{message}",
    description: "QUICKINSERT.SearchChatCommandEmote",
  },
  {
    id: "em",
    commandText: "/em",
    args: "{message}",
    description: "QUICKINSERT.SearchChatCommandEmote",
  },
  {
    id: "me",
    commandText: "/me",
    args: "{message}",
    description: "QUICKINSERT.SearchChatCommandEmote",
  },
  {
    id: "whisper",
    commandText: "/whisper",
    args: "{target} {message}",
    description: "QUICKINSERT.SearchChatCommandWhisper",
  },
  {
    id: "w",
    commandText: "/w",
    args: "{target} {message}",
    description: "QUICKINSERT.SearchChatCommandWhisper",
  },
  {
    id: "roll",
    commandText: "/roll",
    args: "{dice formula}",
    description: "QUICKINSERT.SearchChatCommandRoll",
  },
  {
    id: "r",
    commandText: "/r",
    args: "{dice formula}",
    description: "QUICKINSERT.SearchChatCommandRoll",
  },
  {
    id: "gmroll",
    commandText: "/gmroll",
    args: "{dice formula}",
    description: "CHAT.RollPrivate",
  },
  {
    id: "gmr",
    commandText: "/gmr",
    args: "{dice formula}",
    description: "CHAT.RollPrivate",
  },
  {
    id: "blindroll",
    commandText: "/blindroll",
    args: "{target}",
    description: "CHAT.RollBlind",
  },
  {
    id: "broll",
    commandText: "/broll",
    args: "{target}",
    description: "CHAT.RollBlind",
  },
  {
    id: "br",
    commandText: "/br",
    args: "{target}",
    description: "CHAT.RollBlind",
  },
  {
    id: "selfroll",
    commandText: "/selfroll",
    args: "{dice formula}",
    description: "CHAT.RollSelf",
  },
  {
    id: "sr",
    commandText: "/sr",
    args: "{dice formula}",
    description: "CHAT.RollSelf",
  },
  {
    id: "publicroll",
    commandText: "/publicroll",
    args: "{dice formula}",
    description: "CHAT.RollPublic",
  },
  {
    id: "pr",
    commandText: "/pr",
    args: "{dice formula}",
    description: "CHAT.RollPublic",
  },
].reverse();

export class SlashController extends SearchController {
  #input = "";
  callbacks: SearchControllerOptions;

  constructor(callbacks: SearchControllerOptions) {
    super();
    this.callbacks = callbacks;
  }

  #onExampleSelected(commandText: string) {
    this.callbacks.setInputText(commandText);
  }

  onTab(item: SearchResultItem): void {
    if (item.item.commandText !== this.#input) {
      this.#onExampleSelected(item.item.commandText);
    }
  }

  onAction(item: { commandText: string }): void {
    if (item.commandText === this.#input) {
      if (item.commandText === "/" || item.commandText.startsWith("/ ")) return;

      ui.chat
        //@ts-expect-error Don't care about protected
        .processMessage(item.commandText)
        .catch((error) => ui.notifications.error(error));
      this.callbacks.close();
    } else {
      this.#onExampleSelected(item.commandText);
    }
  }

  search(textInput: string): SearchResultItem[] {
    this.#input = textInput;
    const cleanedInput = textInput.replace("/", "").toLowerCase().trim();
    return [
      ...slashCommands
        .filter((item) => item.commandText.includes(cleanedInput))
        .map((item) => ({ item })),
      { item: { commandText: textInput, id: "textInput" } },
    ];
  }
}

interface CalcResult {
  input: string;
  result?: number;
  history?: true;
}
export class CalcController extends SearchController {
  #history: { item: CalcResult }[] = [];
  setInputText: (inputText: string) => void;

  constructor(callbacks: SearchControllerOptions) {
    super();
    this.setInputText = callbacks.setInputText;
  }
  onTab(): void {}

  onAction(item: CalcResult): void {
    if (item.result === undefined) {
      return;
    }

    this.#history.push({ item: { ...item, history: true } });
    this.setInputText("=" + item.result.toString());
  }

  search(textInput: string): SearchResultItem[] {
    const cleanedInput = textInput.replace("=", "").toLowerCase().trim();

    if (!cleanedInput) {
      return [];
    }

    let result: number | undefined;

    try {
      result = calculate(cleanedInput);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (error) {
      // Not useful
    }

    if (result === undefined || isNaN(result)) {
      result = undefined;
    }

    const item: CalcResult = {
      input: cleanedInput,
      result,
    };

    return [...this.#history, { item }];
  }
}

export class AudioController extends SearchController {
  static #stoppedActions: DocumentAction[] = [
    {
      id: "play",
      icon: "fas fa-play",
      title: "PLAYLIST.Play",
    },
  ];
  static #playingActions: DocumentAction[] = [
    {
      id: "previous",
      icon: "fas fa-backward",
      title: "PLAYLIST.Backward",
    },
    {
      id: "next",
      icon: "fas fa-forward",
      title: "PLAYLIST.Forward",
    },
    {
      id: "stop",
      icon: "fas fa-stop",
      title: "PLAYLIST.Stop",
    },
  ];
  #options: SearchControllerOptions;

  constructor(options: SearchControllerOptions) {
    super();
    this.#options = options;
  }
  onTab(): void {}

  onAction(item: { instance: Playlist }, action?: string): void {
    switch (action) {
      case "play":
        item.instance.playAll();
        break;
      case "stop":
        item.instance.stopAll();
        break;
      case "next":
        item.instance.playNext(undefined, { direction: 1 });
        break;
      case "previous":
        item.instance.playNext(undefined, { direction: -1 });
        break;
      default:
        if (item.instance.playing) {
          item.instance.stopAll();
        } else {
          item.instance.playAll();
        }
        break;
    }
    Hooks.once("updatePlaylist", () => this.#options.refresh());
  }

  search(textInput: string): SearchResultItem[] {
    const cleanedInput = textInput.replace("#", "").toLowerCase().trim();
    return searchPlaylists(cleanedInput).map((item) => ({
      ...item,
      defaultAction: item.item.instance.playing ? "stop" : "play",
      actions: item.item.instance.playing
        ? AudioController.#playingActions
        : AudioController.#stoppedActions,
    }));
  }
}

export const modeConfig: Record<
  SearchMode,
  {
    showInInsertMode?: true;
    prefix?: string;
    controller: (options: SearchControllerOptions) => SearchController;
    // TODO: better type common props!
    component: Component<any>;
  }
> = {
  [SearchMode.DOCUMENT]: {
    showInInsertMode: true,
    controller: (options) => new DocumentController(options),
    component: SearchResults,
  },
  [SearchMode.FILTER]: {
    showInInsertMode: true,
    prefix: "@",
    controller: (options) => new FilterController(options),
    component: SearchFiltersResults,
  },
  [SearchMode.HELP]: {
    prefix: "?",
    controller: (options) => new HelpController(options),
    component: SearchHelpResults,
  },
  [SearchMode.COMMANDS]: {
    prefix: ">",
    controller: (options) => new KeybindController(options),
    component: SearchCommandResults,
  },
  [SearchMode.SLASH]: {
    prefix: "/",
    controller: (options) => new SlashController(options),
    component: SearchSlashResults,
  },
  [SearchMode.CALC]: {
    prefix: "=",
    controller: (options) => new CalcController(options),
    component: SearchCalcResults,
  },
  [SearchMode.AUDIO]: {
    prefix: "#",
    controller: (options) => new AudioController(options),
    component: SearchAudioResults,
  },
};
