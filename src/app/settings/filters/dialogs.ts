import { mloc } from "module/utils";
import { FilterType, type SearchFilter } from "module/store/Filters";
import { deleteFilter } from "module/store/FilterStore";
import { createFilter } from "./filterUtils";

export function newDialog(
  original?: SearchFilter,
  createdCallback?: (newId: string) => void,
  type = FilterType.World
) {
  new Dialog({
    title: original
      ? mloc("FilterListDuplicateFilterTitle", { original: original.tag })
      : mloc("FilterListNewFilterTitle"),
    content: `
        <div class="new-filter-name">
          @<input type="text" name="name" id="name" value="" placeholder="${mloc(
            "FilterListFilterTagPlaceholder"
          )}" pattern="[A-Za-z0-9\\._\\-]+" minlength="1" maxlength="36">
        </div>
      `,
    buttons: {
      apply: {
        icon: "<i class='fas fa-plus'></i>",
        label: mloc("FilterListCreateFilter"),
        callback: async (
          html: JQuery<HTMLElement> | HTMLElement
        ): Promise<void> => {
          if (!("find" in html)) return;
          const input = html.find("input");
          const val = html.find("input").val() as string;
          if (input.get(0)?.checkValidity() && val !== "") {
            const newId = createFilter(val, type, original);
            setTimeout(() => createdCallback?.(newId), 0);
          } else {
            ui.notifications?.error(`Incorrect filter tag: "${val}"`);
          }
        },
      },
    },
    default: "apply",
    close: (): void => {
      return;
    },
  }).render(true);
}

export function deleteDialog(
  filter: SearchFilter,
  deleteCallback?: () => void
) {
  new Dialog({
    title: `Delete filter ${filter.tag}`,
    content: `<p>Delete filter <strong>@${filter.tag} - ${filter.subTitle}</strong> permanently?</p>`,
    buttons: {
      delete: {
        icon: '<i class="fas fa-trash"></i>',
        label: "Delete",
        callback: () => {
          deleteCallback?.();
          deleteFilter(filter.id);
        },
      },
      cancel: {
        label: "Cancel",
        callback: () => {
          return;
        },
      },
    },
    default: "delete",
  }).render(true);
}
