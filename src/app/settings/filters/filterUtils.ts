import { mloc, randomId } from "module/utils";

import {
  cloneFilterConfig,
  FilterType,
  FILTER_COMPENDIUM_ALL,
  FILTER_FOLDER_ROOT,
  type SearchFilter,
  type SearchFilterConfig,
} from "module/store/Filters";
import { typeLabel } from "module/typeUtils";
import { addFilter } from "module/store/FilterStore";

export function getCompendiumTitle(compendiums: string[]) {
  if (!compendiums.length) {
    return mloc("FilterEditorCompendium");
  }

  if (compendiums[0] === FILTER_COMPENDIUM_ALL) {
    return mloc("FilterEditorCompendiumAll");
  }

  if (compendiums.length === 1 && compendiums[0].length < 20) {
    return game.packs.get(compendiums[0])?.title || compendiums[0];
  }
  return (
    (compendiums.length === 1 && game.packs?.get(compendiums[0])?.title) ||
    mloc("FilterEditorCompendiumMany", {
      packCount: compendiums.length.toString(),
    })
  );
}

export function getFolderTitle(folders: string[]) {
  if (!folders.length) {
    return mloc("FilterEditorDirectory");
  }

  if (folders[0] === FILTER_FOLDER_ROOT) {
    return mloc("FilterEditorFolderRoot");
  }

  if (folders.length === 1) {
    return game.folders?.get(folders[0])?.name || folders[0];
  }

  return (
    (folders.length === 1 && game.folders?.get(folders[0])?.name) ||
    mloc("FilterEditorFolderMany", {
      folderCount: folders.length.toString(),
    })
  );
}

// Get labels for what's selected in the config.
export function getLabels(config: SearchFilterConfig) {
  const labels: {
    documentTypes?: string;
    folders?: string;
    packs?: string;
  } = {};

  if (config.documentTypes?.length) {
    if (config.documentTypes.length === 1) {
      labels.documentTypes = typeLabel(config.documentTypes[0]);
    } else {
      labels.documentTypes = mloc("FilterEditorTypeMany", {
        typeCount: config.documentTypes.length.toString(),
      });
    }
  }

  if (config.folders.length) {
    labels.folders = getFolderTitle(config.folders);
  }

  if (config.compendiums.length) {
    labels.packs = getCompendiumTitle(config.compendiums);
  }

  return labels;
}

export function createFilter(
  tag: string,
  type: FilterType,
  original?: SearchFilter
): string {
  console.log("Quick Insert: create filter", tag, type, original);

  const newId = randomId(30);

  const filterConfig = original
    ? cloneFilterConfig(original.filterConfig)
    : { compendiums: [], folders: [], documentTypes: [] };

  const subTitle = original ? `${original.subTitle} (Copy)` : tag;

  addFilter({
    id: newId,
    type,
    tag,
    subTitle,
    filterConfig,
    role:
      type === FilterType.Client
        ? CONST.USER_ROLES.PLAYER
        : original?.role || CONST.USER_ROLES.GAMEMASTER,
  });

  return newId;
}

export function makeEmptyFilter(): SearchFilter {
  const newId = randomId(30);

  const filterConfig = { compendiums: [], folders: [], documentTypes: [] };

  return {
    id: newId,
    type: FilterType.Temporary,
    tag: "~",
    subTitle: mloc("FilterPopupTitle"),
    filterConfig,
    role: CONST.USER_ROLES.PLAYER,
  };
}
