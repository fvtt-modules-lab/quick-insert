import { mount, unmount } from "svelte";
import { type SearchFilter } from "module/store/Filters";
import { makeEmptyFilter } from "app/settings/filters/filterUtils";
import FilterPopup from "./FilterPopup.svelte";

const AppV2 = foundry.applications.api.ApplicationV2;

const WINDOW_WIDTH = 400;
const WINDOW_MARGIN = 5;

export class FilterPopupApp extends AppV2 {
  static DEFAULT_OPTIONS = {
    id: "qi-filter-popup{id}",
    classes: ["application", "qi-filter-popup-app"],
    window: {
      title: "QUICKINSERT.FilterPopupTitle",
      frame: true,
      positioned: true,
      minimizable: true,
      resizable: true,
      content: false,
    },
  };

  static get windowHeight(): number {
    return window.innerHeight / 2 + 30;
  }

  #unmount?: Record<string, any>;
  #filter: SearchFilter;
  #onChange: (filter: SearchFilter) => void;
  #onClose: () => void;

  props: {
    filter: SearchFilter;
  } = $state({ filter: makeEmptyFilter() });

  constructor(
    filter: SearchFilter,
    onChange: (filter: SearchFilter) => void,
    onClose: () => void,
    position?: { left: number; top: number }
  ) {
    const left = position?.left || window.innerWidth / 2 + 250;
    const top = position?.top || WINDOW_MARGIN;
    const options = {
      position: {
        top,
        left,
        width: 400,
        height: FilterPopupApp.windowHeight,
        scale: 1,
        zIndex: _maxZ,
      },
    };
    super(options);
    this.#filter = filter;
    this.#onChange = onChange;
    this.#onClose = onClose;
  }

  protected _onClose(): void {
    this.#onClose();
    if (this.#unmount) {
      unmount(this.#unmount, { outro: false });
    }
  }

  override async _renderHTML(): Promise<string> {
    return "";
  }

  override _replaceHTML(result: string, content: HTMLElement) {
    this.props.filter = this.#filter;

    this.#unmount = mount(FilterPopup, {
      target: content,
      props: this.props,
      events: {
        close: () => this.close(),
        change: (event) => {
          const modifiedFilter: SearchFilter = {
            ...this.#filter,
            filterConfig: event.detail,
          };
          this.props.filter = modifiedFilter;
          this.#onChange(modifiedFilter);
        },
      },
    });
  }
}

export async function openFilterPopup(
  filter: SearchFilter,
  onChange: (filter: SearchFilter) => void,
  onClose: () => void,
  position: DOMRect
): Promise<FilterPopupApp> {
  const rightEdge = position.right + WINDOW_MARGIN;
  const leftEdge = position.left - WINDOW_WIDTH - WINDOW_MARGIN;
  const left =
    rightEdge + WINDOW_WIDTH + WINDOW_MARGIN * 2 > screen.width
      ? leftEdge
      : rightEdge;

  const top = position.bottom - FilterPopupApp.windowHeight;

  const app = new FilterPopupApp(filter, onChange, onClose, { left, top });
  await app.render(true);
  return app;
}
