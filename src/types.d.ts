// Extra types not covered by foundry-vtt-types

type ValueOf<T> = T[keyof T];

// Instead of importing complex helpers
declare type AnyDocumentType =
  | typeof Actor
  | typeof Item
  | typeof JournalEntry
  | typeof Macro
  | typeof RollTable
  | typeof Scene;

declare type AnyDocument = InstanceType<AnyDocumentType>;

declare class GenericCollection extends WorldCollection<AnyDocumentType, any> {}

interface AssumeHookRan {
  i18nInit: never;
  ready: never;
}

interface LenientGlobalVariableTypes {
  game: never;
  ui: never;
}

declare namespace KeyboardManager {
  interface KeyboardEventContext {
    /** Custom field */
    _quick_insert_extra?: unknown;
  }
}
declare namespace ClientKeybindings {
  interface KeybindingActionConfig {
    /** Custom field */
    textInput?: boolean;
  }
}
interface FolderTreeNode<T> {
  folder: Folder;
  children: T[];
  selected: boolean;
}

type FolderTree = FolderTreeNode<FolderTree>;

interface SettingConfig {
  "quick-insert.wip": boolean;
}
