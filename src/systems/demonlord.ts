// Shadow of the Demon Lord integration
import { CharacterSheetContext } from "module/contexts";
import { QuickInsert } from "module/core";
import { getSetting } from "module/settings";
import { ModuleSetting } from "module/store/ModuleSettings";

export const SYSTEM_NAME = "demonlord";

export class DemonLordSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet<any, any>,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(documentSheet, anchor, insertType ? [insertType] : undefined);
  }
}

export function demonlordRenderHook(
  app: DocumentSheet<any, any>,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  const link = `<a class="item-control quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create,a.spell-create").each((i, el) => {
    const linkEl = $(link);
    $(el).after(linkEl);
    const type = el.dataset.type;
    linkEl.on("click", () => {
      const context = new DemonLordSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });

  app.element.find(".ancestry-frame ~ h3").each((i, el) => {
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new DemonLordSheetContext(
        app,
        linkEl,
        sheetType,
        "ancestry"
      );
      QuickInsert.open(context);
    });
  });

  app.element.find(".path-frame ~ h3").each((i, el) => {
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new DemonLordSheetContext(app, linkEl, sheetType, "path");
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  Hooks.on("renderDLCharacterSheet", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      demonlordRenderHook(app, "character");
    }
  });
  Hooks.on("renderDLCreatureSheet", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      demonlordRenderHook(app, "creature");
    }
  });

  console.log("Quick Insert | demonlord system extensions initiated");
}
