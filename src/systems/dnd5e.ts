// D&D 5th edition integration
import { systemFields } from "module/systemFields";
import { CharacterSheetContext } from "module/contexts";
import { QuickInsert } from "module/core";
import { getSetting } from "module/settings";
import { ModuleSetting } from "module/store/ModuleSettings";
import { DocumentType } from "module/searchLib";

export const SYSTEM_NAME = "dnd5e";

export class Dnd5eSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet<any, any>,
    anchor: JQuery<HTMLElement>,
    insertType?: string[],
    system?: Record<string, unknown>
  ) {
    super(documentSheet, anchor, insertType, system);
  }
}

const ignoredTypes = new Set(["passengers", "crew"]);
const listTypeMap: Record<string, string[] | undefined> = {
  spellbook: ["spell"],
  features: ["feat"],
};
const itemTypeMap: Record<string, string[] | undefined> = {
  all: ["weapon", "equipment", "consumable", "loot"],
};

const linkHtml = `<a class="item-control quick-insert-link" aria-label="Quick Insert" data-tooltip="Quick Insert"><i class="fas fa-search"></i></a>`;

function sheetNew5eRenderHook(app: DocumentSheet<any, any>): void {
  if (!app.isEditable || app.element.find(".quick-insert-link").length > 0) {
    return;
  }

  app.element.find(`div.items-section`).each((i, section) => {
    const itemType = section.dataset.type;
    const listType = section.parentElement?.dataset.itemList;

    if (!(itemType && listType)) {
      return;
    }

    if (itemType && ignoredTypes.has(itemType)) {
      return;
    }

    const types = listTypeMap[listType] || itemTypeMap[itemType] || [itemType];

    let system: Record<string, unknown> | undefined;
    if (types.length === 1 && types[0] === "spell" && section.dataset.level) {
      system = { level: parseInt(section.dataset.level, 10) };
    }

    const linkEl = $(linkHtml);
    $(section).find(".items-header .item-controls").append(linkEl);
    linkEl.on("click", () => {
      const context = new Dnd5eSheetContext(app, linkEl, types, system);
      QuickInsert.open(context);
    });
  });
}

function sheetLegacy5eRenderHook(app: DocumentSheet<any, any>): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  app.element.find("a.item-create").each((i, el) => {
    let type = el.dataset.type;
    if (!type) {
      let parent = el.parentElement;
      while (parent && parent !== app.element[0]) {
        if (parent.dataset.type) {
          type = parent.dataset.type;
          break;
        }
        parent = parent.parentElement;
      }
    }

    if (type && ignoredTypes.has(type)) {
      return;
    }

    const linkEl = $(linkHtml);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new Dnd5eSheetContext(
        app,
        linkEl,
        type ? [type] : undefined
      );
      QuickInsert.open(context);
    });
  });
}

function sheet5eRenderHook(app: DocumentSheet<any, any>) {
  if (!getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
    return;
  }

  if (app.element.hasClass("dnd5e2")) {
    sheetNew5eRenderHook(app);
  } else {
    sheetLegacy5eRenderHook(app);
  }
}

export function init(): void {
  systemFields.push({
    documentType: DocumentType.ITEM,
    indexName: "level",
    fieldTitle: "Level",
  });

  Hooks.on("renderActorSheet5eCharacter", sheet5eRenderHook);
  Hooks.on("renderActorSheet5eNPC", sheet5eRenderHook);
  Hooks.on("renderActorSheet5eVehicle", sheet5eRenderHook);
  Hooks.on("renderTidy5eSheet", sheet5eRenderHook);
  Hooks.on("renderTidy5eNPC", sheet5eRenderHook);

  try {
    type MaybeApi = { api?: any };

    const tidyApi =
      (game.modules.get("tidy5e-sheet") as MaybeApi)?.api ??
      (game.modules.get("tidy5e-sheet-kgar") as MaybeApi)?.api;

    if (tidyApi) {
      tidyApi.actorItem.registerSectionFooterCommands([
        {
          enabled: (params: any) =>
            getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED) &&
            ["npc", "character", "vehicle"].includes(params.actor.type),
          execute: (params: any) => {
            const context = new Dnd5eSheetContext(
              params.actor.sheet,
              $(params.event.currentTarget),
              [params.section.dataset.type],
              params.section.dataset.system
            );
            QuickInsert.open(context);
          },
          iconClass: "fas fa-search",
          tooltip: "Quick Insert",
        },
      ]);
    }
  } catch (e) {
    console.error(
      "Tidy 5e Sheet (Rewrite) Quick Insert compatibility failed to initialize",
      e
    );
  }

  console.log("Quick Insert | dnd5e system extensions initiated");
}
