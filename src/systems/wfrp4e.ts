// Warhammer Fantasy Roleplay 4th edition integration
import { CharacterSheetContext } from "module/contexts";
import { QuickInsert } from "module/core";
import { getSetting } from "module/settings";
import { ModuleSetting } from "module/store/ModuleSettings";

export const SYSTEM_NAME = "wfrp4e";

export class Wfrp4eSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet<any, any>,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(documentSheet, anchor, insertType ? [insertType] : undefined);
    this.spawnCSS = {
      ...this.spawnCSS,
      left: (this.spawnCSS?.left as number) - 10,
      bottom: (this.spawnCSS?.bottom as number) + 10,
    };
  }
}

export function sheetWfrp4eRenderHook(
  app: DocumentSheet<any, any>,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    if (!Object.keys(CONFIG.Item.typeLabels).includes(type)) return;
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new Wfrp4eSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  Hooks.on(
    "renderActorSheetWfrp4eCharacter",
    (app: DocumentSheet<any, any>) => {
      if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
        sheetWfrp4eRenderHook(app, "character");
      }
    }
  );

  console.log("Quick Insert | wfrp4e system extensions initiated");
}
