// Savage Worlds Adventure Edition integration
import { CharacterSheetContext } from "module/contexts";
import { QuickInsert } from "module/core";
import { getSetting } from "module/settings";
import { ModuleSetting } from "module/store/ModuleSettings";

import type { SearchItem } from "module/searchLib";

export const SYSTEM_NAME = "swade";
const ignoredTypes = new Set(["advance", "choice"]);

export class SwadeSheetContext extends CharacterSheetContext {
  equipped = false;
  constructor(
    documentSheet: DocumentSheet<any, any>,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string,
    equipped?: boolean
  ) {
    super(documentSheet, anchor, insertType ? [insertType] : undefined);
    this.equipped = Boolean(equipped);
  }
  onSubmit(item: SearchItem | string): Promise<AnyDocument[]> | undefined {
    const res = super.onSubmit(item);
    if (this.equipped && res) {
      res.then((items) => {
        const item = items.length && items[0];
        if (!item) return;

        //@ts-expect-error Lacking system types
        if (item?.data?.equippable) {
          item.update({ "data.equipped": true });
        }
      });
    }

    return res;
  }
}

export function sheetSwadeRenderHook(
  app: DocumentSheet<any, any>,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }

  // Legacy sheets
  const link = `<a class="quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element.find("a.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    if (type && ignoredTypes.has(type)) {
      return;
    }
    const equipped = el.dataset.equipped === "true";
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(
        app,
        linkEl,
        sheetType,
        type,
        equipped
      );
      QuickInsert.open(context);
    });
  });

  // New character sheet
  app.element.find("button.item-create").each((i, el) => {
    const type = el.dataset.type || "";
    const linkEl = $(link);
    $(el).after(linkEl);
    linkEl.on("click", () => {
      const context = new SwadeSheetContext(app, linkEl, sheetType, type);
      QuickInsert.open(context);
    });
  });
}

export function init(): void {
  Hooks.on("renderCharacterSheet", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      sheetSwadeRenderHook(app, "character");
    }
  });
  Hooks.on("renderSwadeNPCSheet", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      sheetSwadeRenderHook(app, "npc");
    }
  });
  Hooks.on("renderSwadeVehicleSheet", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      sheetSwadeRenderHook(app, "vehicle");
    }
  });

  console.log("Quick Insert | swade system extensions initiated");
}
