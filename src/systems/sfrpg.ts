// Starfinder integration
import { CharacterSheetContext } from "module/contexts";
import { QuickInsert } from "module/core";
import { getSetting } from "module/settings";
import { ModuleSetting } from "module/store/ModuleSettings";

export const SYSTEM_NAME = "sfrpg";

export class SfrpgSheetContext extends CharacterSheetContext {
  constructor(
    documentSheet: DocumentSheet<any, any>,
    anchor: JQuery<HTMLElement>,
    sheetType?: string,
    insertType?: string
  ) {
    super(documentSheet, anchor, insertType ? [insertType] : undefined);
  }
}

export function sheetSfrpgRenderHook(
  app: DocumentSheet<any, any>,
  sheetType?: string
): void {
  if (app.element.find(".quick-insert-link").length > 0) {
    return;
  }
  const link = `<a class="item-control quick-insert-link" title="Quick Insert"><i class="fas fa-search"></i></a>`;
  app.element
    .find("a.item-create, .item-control.spell-browse")
    .each((i, el) => {
      const linkEl = $(link);
      $(el).after(linkEl);
      const type = el.dataset.type;
      linkEl.on("click", (evt) => {
        evt.stopPropagation();
        const context = new SfrpgSheetContext(app, linkEl, sheetType, type);
        QuickInsert.open(context);
      });
    });
}

export function init(): void {
  Hooks.on("renderActorSheetSFRPGCharacter", (app: DocumentSheet<any, any>) => {
    if (getSetting(ModuleSetting.FILTERS_SHEETS_ENABLED)) {
      sheetSfrpgRenderHook(app, "character");
    }
  });

  console.log("Quick Insert | sfrpg system extensions initiated");
}
