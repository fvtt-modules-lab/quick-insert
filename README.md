![Quick Insert Header](https://i.imgur.com/es7tsOK.gif)

# Quick Insert - Find and insert anything you need

[![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fquick-insert&colorB=4aa94a)](https://forge-vtt.com/bazaar#package=quick-insert)
[![Translation status](https://weblate.foundryvtt-hub.com/widgets/quick-insert/-/main/svg-badge.svg)](https://weblate.foundryvtt-hub.com/engage/quick-insert/)

[Discord](https://sunspots.eu/chat) |
[Quick Insert on Foundry Packages](https://foundryvtt.com/packages/quick-insert) |
[Support on Ko-Fi](https://ko-fi.com/sunspots) |
[Support on Patreon](https://www.patreon.com/sunspots)

A powerful tool for finding, controlling and inserting things in Foundry VTT. **Quick Insert** brings what you need to your fingertips; always showing the best match first, reducing the amout of time needed scrolling through search results.

- Find any actor, item, spell etc. and insert it anywhere applicable; the map, character sheets, roll tables, journals, you name it.
- Run commands, calculate numbers, control playlists, etc. on-the-fly.

### Using Quick Insert:

- **Open with <kbd>Ctrl</kbd> + <kbd>Space</kbd>** (editable under "Configure Controls").

  - If you focus an input field first, Quick Insert will attach to it - automatically inserting results from your search.
  - Quick Insert will use selected text to initiate the search.

- **Open from character sheets.** Quick Insert adds buttons to select character sheets. Quick Insert will automatically filter the search results based on the section. E.g. searching for cantrips in the cantrip section.
- **Close with <kbd>Escape</kbd>** or by clicking outside the window.
- **Navigate the results with the arrow keys <kbd>↑</kbd> <kbd>↓</kbd>.**
- Select results with <kbd>Enter</kbd> or mouse click.
- **Select other actions with <kbd>Tab</kbd>** or by clicking the buttons on the right.
  - Roll roll-tables.
  - Execute macros.
  - Activate scenes.
  - And more...
- **Hold <kbd>Shift</kbd>** to keep the search open, so you can
  select more than one result!
- Drag-drop directly from the search results.
- **Type `@` to use search filters**. Select the filter with <kbd>Tab</kbd>, <kbd>Enter</kbd> or click.
- Find more available modes by typing `?`.

### More Commands & Controls

- **Show help and all available controls with `?`**
- **Control playlists with `#`.** Play, pause or change tracks in playlists.
- **Use a calculator with `=`.** The calculator supports parenthesis and some functions, like sin/cos/tan/log.
- **Run chat slash commands with `/`, like `/roll`.**
- **Find and run any keyboard command with `>`.**

## Examples

[Album with more examples](https://imgur.com/a/mHv88KW)

![Drag-drop things](https://i.imgur.com/sFTBYQw.gif)
![Insert in character sheets](https://i.imgur.com/PfaoB3g.gif)
![Use filters](https://i.imgur.com/ORegNUd.gif)
![Insert in character sheet](https://i.imgur.com/PfaoB3g.gif)
![Filter editor](https://i.imgur.com/bYBExRy.png)

## Installation

Quick Insert is available in the in-application module browser. It can also be
installed manually with the following manifest URL:

```
https://gitlab.com/fvtt-modules-lab/quick-insert/-/jobs/artifacts/master/raw/module.json?job=build-module
```

### Unstable branch

If you want to live on the edge, you can use the latest version from the
`develop` branch, but there may be unfinished features and rough edges.

```
https://gitlab.com/fvtt-modules-lab/quick-insert/-/jobs/artifacts/develop/raw/module.json?job=build-unstable
```

## Search sources

Quick Insert lets you search both among entities in your world, as well as
entities in any of your compendiums. Search for **Actors, Items, Journals,
Macros, Rollable Tables or Scenes**.

You can configure who is allowed to search for which type of entities, as well
as in which compendiums by using the "Indexing settings" menu.

## Integrated search context - Insert mode

Quick Insert can attach itself to a certain **"context"** for automatic
insertion. As of now, these are:

- Journals and other rich text editors, via keyboard shortcut or Quick
  Insert button in the tool bar - supports quick search of selected text!
- Macro editor (both script and chat macros)
- Chat (Note: @ tags may conflict with mentions when it's in the start of a
  message)
- Rollable table editor - automatically updates entire row when searching in
  text field
- Dnd5e and Tidy5e character and NPC sheets have search buttons added
- Modules may spawn a Quick Insert prompt with a custom context (PM me on
  Discord if you want help to integrate)

## Developers & API

Modules are free to invoke Quick Insert, although incompatible changes may occur
without notice, breaking compatibility.

### `QuickInsert.open(context)`

Open the Quick Insert search app, with an optional context to attach to. If you
add a "context" to the Quick Insert search app, you can control how you want the
search to behave.

```JavaScript
QuickInsert.open({
  // All fields are optional
  spawnCSS: {
    // This entire object is passed to JQuery .css()
    // https://api.jquery.com/css/#css-properties
    left: 600,
    top: 100,
  };
  classes: [ "myClass" ],  // A list of classes to be added to the search app
  filter: "dnd5e.items",   // The name of a filter to pre-fill the search
  startText: "bla",        // The search input will be pre-filled with this text
  allowMultiple: true,     // Unless set to `false`, the user can submit multiple times using the shift key
  restrictTypes: ["Item"], // Restrict the output to these document types only
  onSubmit: (item) => {
    // Do something with the selected item, e.g.
    item.show();
  },
  onClose: () => {
    // You can do something when the search app is closed
  }
})
```

### `QuickInsert.search(text, filter = null, max = 100)`

Use the internal search library from Quick Insert to look up some search
results.

### `QuickInsert.forceIndex()`

Forces Quick Insert to rebuild the entire search index. Useful for testing.

### `QuickInsert.handleKeybind(event, context)`

Handles a keyboard event that could not be handled by Foundry Core, e.g. from a custom text editor. Requires a search context, see `QuickInsert.open(context)`.

### `QuickInsert.hasIndex`

True if QuickInsert has an index that can be searched. Gives no guarantees that compendiums have been loaded yet.

### `SearchItem`

The object passed to `onSubmit`, as well as the items in the result from
`QuickInsert.search()` is called `SearchItems`. They have the following
properties:

```JavaScript
item.name
item.id
item.img
item.documentType
// Computed properties
// Get the uuid value compatible with fromUuid
item.uuid
// Get the drag data for drag operations
item.dragData
// Get the html for an icon that represents the item
item.icon
// Reference the document in a journal, chat or other places that support it
item.journalLink
// Reference the document in a script
item.script
// Short tagline that explains where/what this is
item.tagline
// (async) Show the sheet or equivalent of this search result
item.show()
// Fetch the original object (or null if no longer available).
// NEVER call as part of indexing or filtering.
// It can be slow and most calls will cause a request to the database!
// Call it once a decision is made, do not call for every SearchItem!
item.get()
```

### Hooks

#### `QuickInsert:IndexCompleted`

Called when the index (including compendiums) have been (re-)built.

### "Debug mode"

You can force QuickInsert into "debug mode" by setting
`QuickInsert.app.debug = true`.

- Quick Insert will stay open even when it loses focus.
- More debug features may be added in the future.

## System and character sheet integration

While developers can use the API themselves to control Quick Insert, there is
also some integration built into Quick Insert. If you have a system or a certain
character sheet variant, and want Quick Insert integration, feel free to create
a new issue here on GitLab.
